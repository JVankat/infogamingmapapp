package de.htw_dresden.infogamingmapapp;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.backendless.exceptions.BackendlessException;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.model.Team;
import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.service.network.TeamService;
import de.htw_dresden.infogamingmapapp.service.network.UserService;

@EActivity
public class TeamActivity extends BaseActivity {

    Team mTeam;
    TextView mTeamNameTv;
    ListView mMembersLv;
    List<String> mMemberNames = new ArrayList<>();
    ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        mTeam = (Team) getIntent().getSerializableExtra("team");
        Log.i(App.TAG, "Team " + mTeam.getName() + " with " + mTeam.memberIds.size() + " members is shown");

        mTeamNameTv = (TextView) findViewById(R.id.teamNameTv);
        mMembersLv = (ListView) findViewById(R.id.membersLv);

        mTeamNameTv.setText(mTeam.getName());
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mMemberNames);
        mMembersLv.setAdapter(mAdapter);
        fetchTeamMembers(mTeam.memberIds);
    }

    @Background
    public void fetchTeamMembers(Set<String> memberIds) {
        Collection<User> users = UserService.getUsers(memberIds);
        List<String> userNames = new ArrayList<>();
        for (User user : users) {
            userNames.add(user.getUsername());
        }
        onTeamMembersFetched(userNames);
    }

    @UiThread
    public void onTeamMembersFetched(Collection<String> memberNames) {
        mMemberNames.clear();
        mMemberNames.addAll(memberNames);
        mAdapter.notifyDataSetChanged();
    }

    @Background
    public void fetchHighScore() {
        //TODO: fetch all game instances and get highest score
    }

    @Background
    public void leaveTeam() {
        try {
            TeamService.leaveTeam(mApp.getCurrentUser().getId(), mTeam.getId());
            onTeamLeft();
        } catch (BackendlessException e) {
            onTeamLeaveFailed(e.getLocalizedMessage());
        }
    }

    @UiThread
    public void onTeamLeaveFailed(String message) {
        showDialog("We've failed you! Could not leave team: " + message);
    }

    @UiThread
    public void onTeamLeft() {
        showToast("You are no longer member of team " + mTeam.getName() + " :'(");
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_leave_team) {
            leaveTeam();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_team, menu);
        return true;
    }
}
