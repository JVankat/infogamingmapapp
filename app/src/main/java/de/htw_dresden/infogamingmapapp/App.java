package de.htw_dresden.infogamingmapapp;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.backendless.Backendless;
import com.google.android.gms.maps.model.LatLng;

import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.service.LocationService;

/**
 * Created by vanka on 28.11.2016.
 */

public class App extends Application {
    public static final String TAG = "3D-PROG";
    public static final String version = "v1";
    public static String GCM_SENDER_ID = "archeoapp-1479155244191";

    public static String PACKAGE_NAME = "de.htw_dresden.infogamingmapapp";

    Location location = null;
    Boolean locationEnabled = null;
    User mCurrentUser = null;


    @Override
    public void onCreate() {
        super.onCreate();
        Backendless.initApp(this, "28A0263B-CD54-B059-FF3F-5D22CDA86400", "97004532-35D8-0901-FFE5-1D351590DD00", App.version);
    }


    public boolean registerLocationListener() {
        LocationService locationService = new LocationService(getApplicationContext());
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;

        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationService);
            locationEnabled = true;
            return true;

        }
    }


    public void setLocationDisabled() {
        this.locationEnabled = false;
    }


    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        if (location == null) {
            LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }

        return this.location;
    }

    public LatLng getLocationAsLatLng() {
        Location location = getLocation();
        if (location != null) {
            return new LatLng(location.getLatitude(), location.getLongitude());
        }
        return null;
    }

    public Boolean isLocationEnabled() {
        return locationEnabled;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //to support multidex gradle setting for api below 22
        MultiDex.install(this);
    }

    public void setCurrentUser(User currentUser) {
        this.mCurrentUser = currentUser;
    }

    public User getCurrentUser() {
        return mCurrentUser;
    }

    public boolean isLoggedIn() {
        return this.mCurrentUser != null;
    }

    public void sendTaskAvailableNotification(Task task) {
        Intent intent = new Intent(this, TaskActivity_.class);
        intent.putExtra("task", task);
        sendNotification("Du bist an einem interessanten Ort angekommen", task.getName(), intent);
    }

    public void sendNotification(String title, String message, Intent resultIntent) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(message);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity_.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }
}
