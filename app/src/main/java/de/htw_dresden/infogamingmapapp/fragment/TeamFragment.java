package de.htw_dresden.infogamingmapapp.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.htw_dresden.infogamingmapapp.App;
import de.htw_dresden.infogamingmapapp.CreateTeamActivity_;
import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.TeamActivity_;
import de.htw_dresden.infogamingmapapp.TeamInterface;
import de.htw_dresden.infogamingmapapp.adapter.TeamAdapter;
import de.htw_dresden.infogamingmapapp.model.Team;
import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.utils.SecurityUtils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TeamFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TeamFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@EFragment
public class TeamFragment extends Fragment implements TeamInterface {

    private List<Team> mTeams;
    private TeamAdapter mAdapter;
    private App mApp;

    private OnFragmentInteractionListener mListener;
    ScheduledExecutorService scheduler =
            Executors.newSingleThreadScheduledExecutor();

    public TeamFragment() {
        // Required empty public constructor
    }

    public static TeamFragment newInstance(App app, List<Team> teams) {
        TeamFragment fragment = new TeamFragment();
        fragment.mApp = app;
        fragment.mTeams = teams;
//        fragment.mTeams = new ArrayList<>();
//        fragment.mTeams.addAll(teams);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new TeamAdapter(getContext(), R.layout.team_item, mTeams);
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                refetchTeams();
            }
        }, 5, 5, TimeUnit.SECONDS);
    }

    @UiThread
    public void refetchTeams() {
//        mTeams.clear();
        List<Team> teams = mListener.getTeams();
        Log.i(App.TAG, teams.size() + " teams got in listener");
//        if (teams != null) {
//            mTeams.addAll(teams);
//            mAdapter.notifyDataSetChanged();
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_team, container, false);
        ListView lv = (ListView) view.findViewById(R.id.teamList);
        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Team team = mAdapter.getItem(i);
                if (team == null) {
                    mListener.showToast("Sorry, team seems to be unavailable.");
                    return;
                }

                if (mListener.isMemberOfTeam(team.getId())) {
                    Log.i(App.TAG, "User is already member of team " + team.getName());
                    Intent intent = new Intent(getContext(), TeamActivity_.class);
                    intent.putExtra("team", team);
                    startActivity(intent);
                    return;
                }
                if (team.memberIds != null && team.memberIds.size() >= team.getMemberThreshold()) {
                    mListener.showToast("Sorry, team seems to be full.");
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final EditText passwordInput = new EditText(getContext());
                if (!team.isPublic()) {
                    passwordInput.setHint("This team requests password");
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    passwordInput.setLayoutParams(lp);
                    builder.setView(passwordInput);
                }
                builder.setTitle("Join team?");
                builder.setNegativeButton("Ugh, nope!", null);
                builder.setPositiveButton("Join", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.i(App.TAG, "User wants to join team " + team.getName());
                        if (!team.isPublic()) {
                            String password = passwordInput.getText().toString();
                            String encryptedPassword = SecurityUtils.encodeSha1(password);
                            if (!team.getShaEncryptedPassword().equals(encryptedPassword)) {
                                Log.i(App.TAG, "But provided wrong password...");
                                mListener.showToast("Nope. Not that way. Failure! Try again.");
                                return;
                            }
                        }
                        mListener.joinTeam(team.getId());
                    }
                });
                builder.create().show();
            }
        });
        Button createTeamButton = (Button) view.findViewById(R.id.createTeamBtn);
        createTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CreateTeamActivity_.class);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void notifyTeamsUpdated() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            Log.e(App.TAG, "Team adapter is null");
            onDestroyView();
        }
    }


    public interface OnFragmentInteractionListener {
        List<Task> getTasks();
        void joinTeam(String teamId);

        void showToast(String message);

        boolean isMemberOfTeam(String teamId);

        List<Team> getTeams();
    }
}
