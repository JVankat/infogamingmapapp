package de.htw_dresden.infogamingmapapp.model;

import java.io.Serializable;

import de.htw_dresden.infogamingmapapp.utils.SecurityUtils;

/**
 * Created by vanka on 16.01.2017.
 */

public class MultipleChoiceQuestion implements Serializable {

    private String id;
    private String title;
    //FUCKING BACKENDLESS DOES NOT WANT TO SAVE THIS
    private StringJsonArray options = new StringJsonArray();
    private StringJsonArray correctAnswers = new StringJsonArray();

    public MultipleChoiceQuestion() {
        this.id = SecurityUtils.generateRandomStringId();
    }

    public MultipleChoiceQuestion(String title, StringJsonArray options, StringJsonArray correctAnswers) {
        this();
        this.title = title;
        this.options = options;
        this.correctAnswers = correctAnswers;
    }

    public StringJsonArray getOptions() {
        return options;
    }

    public void setOptions(StringJsonArray options) {
        this.options = options;
    }

    public StringJsonArray getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(StringJsonArray correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
