package de.htw_dresden.infogamingmapapp.model;

/**
 * Created by vanka on 18.01.2017.
 */

public class TaskToGame {

    private String gameId;
    private String taskId;

    public TaskToGame(){

    }

    public TaskToGame(String taskId, String gameId){
        this.gameId = gameId;
        this.taskId = taskId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
