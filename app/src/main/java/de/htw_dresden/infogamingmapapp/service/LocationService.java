package de.htw_dresden.infogamingmapapp.service;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import de.htw_dresden.infogamingmapapp.App;

/**
 * Created by vanka on 14.11.2016.
 */

public class LocationService implements LocationListener {

    private Context context = null;

    public LocationService(Context context) {
        this.context = context;
    }

    @Override
    public void onLocationChanged(Location location) {
        ((App) context.getApplicationContext()).setLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
