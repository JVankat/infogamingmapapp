package de.htw_dresden.infogamingmapapp.model.task;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import de.htw_dresden.infogamingmapapp.App;
import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.adapter.MultipleChoiceAdapter;
import de.htw_dresden.infogamingmapapp.model.MultipleChoiceQuestion;
import de.htw_dresden.infogamingmapapp.model.StringJsonArray;
import weborb.service.ExcludeProperties;

/**
 * Created by vanka on 09.01.2017.
 */
@ExcludeProperties(propertyNames = {"pin", "owner", "questions"})
public class MultipleChoiceTask extends Task implements Serializable {

    private MultipleChoiceAdapter adapter;
    private ListView listView;
    private TextView questionTextView, descriptionTextView, nameTextView;
    private Button submitButton, nextButton;
    private MultipleChoiceQuestion currentQuestion;
    private List<String> selectedOptions = new ArrayList<>(), currentOptions = new ArrayList<>();
    private Set<String> currentCorrectAnswers = new HashSet<>(); //add items when user input should be validated

    public Double getPointsThreshold() {
        return pointsThreshold;
    }

    public void setPointsThreshold(Double pointsThreshold) {
        this.pointsThreshold = pointsThreshold;
    }

    private List<MultipleChoiceQuestion> questions = new ArrayList<>();

    public List<MultipleChoiceQuestion> getQuestions() {
        return this.questions;
    }

    public void setQuestions(List<MultipleChoiceQuestion> questions) {
        this.questions = questions;
    }

    private Double pointsThreshold;
    private Double pointsPerRightAnswer;
    private Double currentPoints = 0d;

    private Stack<MultipleChoiceQuestion> questionStack = new Stack<>();

    public MultipleChoiceTask() {
        super();
    }

    /**
     * Set points that are awarded for completing this task. Same point ratio is assigned to each correct answer.
     *
     * @param points the points
     */
    public void setPoints(Double points) {
        this.pointsThreshold = points;
    }

    /**
     * Add question to this task
     *
     * @param question      the question
     * @param options       the possible options (must include correct answers)
     * @param correctAnswer the correct answer
     */
    public void addQuestion(String question, Collection<String> options, String correctAnswer) {
        HashSet<String> set = new HashSet<>();
        set.add(correctAnswer);
        addQuestion(question, options, set);
    }

    /**
     * Add question to this task
     *
     * @param question       the question
     * @param options        the possible options (must include correct answers)
     * @param correctAnswers the correct answers
     */
    public void addQuestion(String question, Collection<String> options, Collection<String> correctAnswers) {
        StringJsonArray optionsArray = new StringJsonArray();
        StringJsonArray correctArray = new StringJsonArray();
        for (String option : options) {
            optionsArray.put(option);
        }
        for (String correctOption : correctAnswers) {
            correctArray.put(correctOption);
        }
        this.questions.add(new MultipleChoiceQuestion(question, optionsArray, correctArray));
    }

    @Override
    public void append(Context context, LinearLayout parentView) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.multiple_choice, null);
        parentView.addView(view);
        listView = (ListView) view.findViewById(R.id.choiceItemsLv);
        questionTextView = (TextView) view.findViewById(R.id.questionTv);
        submitButton = (Button) view.findViewById(R.id.submitBtn);
        nextButton = (Button) view.findViewById(R.id.nextBtn);
        nameTextView = (TextView) view.findViewById(R.id.taskName);
        descriptionTextView = (TextView) view.findViewById(R.id.descriptionTv);

        descriptionTextView.setText(this.description);
        nameTextView.setText(this.name);
        nameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(App.TAG, "clicked item: ");
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateCurrentAnswers();
                assignPoints();
                submitButton.setVisibility(View.GONE);
                nextButton.setVisibility(View.VISIBLE);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                renderQuestionOrMarkAsCompleted();
                submitButton.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.GONE);
            }
        });

        questionStack.addAll(questions);

        adapter = new MultipleChoiceAdapter(context, R.layout.multiple_choice_item, currentOptions, selectedOptions, currentCorrectAnswers);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (currentCorrectAnswers.isEmpty()) {
                    String selectedOption = adapter.getItem(i);
                    if (selectedOptions.contains(selectedOption)) {
                        selectedOptions.remove(selectedOption);
                    } else {
                        selectedOptions.add(selectedOption);

                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });

        calculatePointIncrement();
        renderQuestionOrMarkAsCompleted();
    }

    private void assignPoints() {
        if (currentPoints == null || pointsPerRightAnswer == null) {
            return;
        }
        for (String selected : selectedOptions) {
            if (getStringsOfJsonArray(currentQuestion.getCorrectAnswers()).contains(selected)) {
                currentPoints += pointsPerRightAnswer;
            }
        }
    }

    private boolean validateCurrentAnswers() {
        currentCorrectAnswers.addAll(getStringsOfJsonArray(currentQuestion.getCorrectAnswers()));
        adapter.notifyDataSetChanged();
        return true;
    }

    /**
     * Calculate (and store as member variable) how many points are awarded for every correct answer.
     */
    private void calculatePointIncrement() {
        if (pointsThreshold == null) {
            return;
        }
        int correctOptionsCount = 0;
        for (MultipleChoiceQuestion question : questions) {
            correctOptionsCount += question.getCorrectAnswers().length();
        }
        pointsPerRightAnswer = correctOptionsCount != 0 ? (pointsThreshold / (double) correctOptionsCount) : pointsThreshold;
        Log.i(App.TAG, this.description + " awards " + pointsPerRightAnswer + " points per correct answer");
    }

    private void renderQuestionOrMarkAsCompleted() {
        if (!questionStack.isEmpty()) {
            currentQuestion = questionStack.pop();
            questionTextView.setText(currentQuestion.getTitle());
            currentOptions.clear();
            currentOptions.addAll(getStringsOfJsonArray(currentQuestion.getOptions()));
            selectedOptions.clear();
            currentCorrectAnswers.clear();
//            currentCorrectAnswers.addAll(getStringsOfJsonArray(currentQuestion.getCorrectAnswers()));
            adapter.notifyDataSetChanged();
        } else {
            onTaskCompleted(currentPoints);
        }
    }

    private Set<String> getStringsOfJsonArray(StringJsonArray ja) {
        try {
            JSONArray value = new JSONArray(ja.getStringValue());
            Set<String> optionsAsString = new HashSet<>();
            for (int i = 0; i < value.length(); i++) {
                try {
                    optionsAsString.add(value.getString(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return optionsAsString;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new HashSet<>();
    }

}
