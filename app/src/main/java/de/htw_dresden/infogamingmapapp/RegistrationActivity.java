package de.htw_dresden.infogamingmapapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.backendless.exceptions.BackendlessException;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.service.network.UserService;
import de.htw_dresden.infogamingmapapp.utils.NetworkUtils;
import de.htw_dresden.infogamingmapapp.utils.SharedPreferenceHelper;
import de.htw_dresden.infogamingmapapp.utils.ValidationUtils;

import static de.htw_dresden.infogamingmapapp.utils.ValidationUtils.validateNotEmpty;

@EActivity
public class RegistrationActivity extends BaseActivity {

    @ViewById
    EditText usernameEt;
    @ViewById
    EditText emailEt;
    @ViewById
    EditText passwordEt;
    @ViewById
    Button registerBtn;
    private boolean disableApiCalls = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Click(R.id.registerBtn)
    public void onRegisterClick() {
        if(NetworkUtils.isConnectedToNetwork(RegistrationActivity.this)){
            register(usernameEt.getText().toString(), emailEt.getText().toString(), passwordEt.getText().toString());
        }else{
            showToast("Explorer. You are not connected to network. Go kill yourself.");
        }
    }

    @Background
    public void register(String username, String email, String password) {
        if (!validateData(username, email, password)) {
            showInvalidDataDialog();
        } else {
            try {
                if (!disableApiCalls) {
                    disableApiCalls = true;
                    User user = UserService.register(username, email, password, mApp.getLocationAsLatLng());
                    Log.i(App.TAG, user.getUsername());
                    showToast("Registered, please log in!");
                    SharedPreferenceHelper.saveEmail(this, email);
                    SharedPreferenceHelper.savePassword(this, password);
                    Intent intent = new Intent(RegistrationActivity.this, MainActivity_.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

            } catch (BackendlessException e) {
                e.printStackTrace();
                showDialog(e.getLocalizedMessage());
            }
        }
        disableApiCalls = false;
    }

    @UiThread
    public void showInvalidDataDialog() {
        if (!validateNotEmpty(usernameEt.getText().toString(), emailEt.getText().toString(), passwordEt.getText().toString())) {
            String emptyFields = "";
            boolean foundEmpty = false;
            if (emailEt.getText().toString().isEmpty()) {
                emailEt.requestFocus();
                foundEmpty = true;
                emptyFields += "Email";
            }
            if (passwordEt.getText().toString().isEmpty()) {
                if (foundEmpty) {
                    emptyFields += " and ";
                } else {
                    passwordEt.requestFocus();
                }
                emptyFields += "Password";
            }
            if (usernameEt.getText().toString().isEmpty()) {
                if (foundEmpty) {
                    emptyFields += " and ";
                } else {
                    usernameEt.requestFocus();
                }
                emptyFields += "Username";
            }
            emptyFields += " " + "is empty. Try again, failure!";
            showDialog(emptyFields);
        } else {
            showDialog("Invalid email format. Try again, failure!");
        }
    }

    private boolean validateData(String username, String email, String password) {
        return validateNotEmpty(username, email, password) && ValidationUtils.isValidEmail(email);
    }
}
