package de.htw_dresden.infogamingmapapp.model;

import java.io.Serializable;

/**
 * Created by vanka on 21.01.2017.
 */

public class UserToTask implements Serializable {

    private String userId;
    private String taskId;
    private String gameId;

    public UserToTask() {

    }

    public UserToTask(String userId, String taskId, String gameId) {
        this.userId = userId;
        this.taskId = taskId;
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
