package de.htw_dresden.infogamingmapapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.backendless.exceptions.BackendlessException;
import com.backendless.messaging.Message;
import com.google.android.gms.maps.model.LatLng;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.htw_dresden.infogamingmapapp.model.GameInstance;
import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.service.MessagingService;
import de.htw_dresden.infogamingmapapp.service.network.GameService;
import de.htw_dresden.infogamingmapapp.service.network.UserService;
import de.htw_dresden.infogamingmapapp.utils.LocationUtils;

@EActivity
public class GameActivity extends BaseActivity {

    private List<String> mUserNames = new ArrayList<>();

    private TextView mName, mDescription, mDistance, mTimeLimit, mMinPlayers, mMaxPlayers, mHumanReadableLocation, mCurrentScore, mTasksCompleted, mCollectedCollectibles, mState;
    private Button mLeave, mSelectTask, mStartGame;
    private ListView mPlayers;
    private ImageView mAugust;

    private ArrayAdapter<String> mAdapter;
    private List<Message> mMessagesToHandle = new ArrayList<>();

    ScheduledExecutorService gameUpdateScheduler = Executors.newSingleThreadScheduledExecutor();
    ScheduledExecutorService userUpdateScheduler = Executors.newSingleThreadScheduledExecutor();

    private static final Integer GAME_UPDATE_PERIOD_SECONDS = 40;
    private static final Integer USER_UPDATE_PERIOD_SECONDS = 20;
    private static final Integer GAME_UPDATE_OFFSET_SECONDS = 10;
    private static final Integer USER_UPDATE_OFFSET_SECONDS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mCurrentGame = (GameInstance) getIntent().getSerializableExtra("gameInstance");
        mUsers = (List<User>) getIntent().getSerializableExtra("users");
        initUi();
        gameUpdateScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                refetchGameInstance();
            }
        }, GAME_UPDATE_OFFSET_SECONDS, GAME_UPDATE_PERIOD_SECONDS, TimeUnit.SECONDS);

        userUpdateScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                refetchUsers();
            }
        }, USER_UPDATE_OFFSET_SECONDS, USER_UPDATE_PERIOD_SECONDS, TimeUnit.SECONDS);


        renderUi();
        subscribeToChannel();
    }

    private void initUi() {
        mName = (TextView) findViewById(R.id.gameNameTv);
        mDescription = (TextView) findViewById(R.id.gameDescriptionTv);
        mDistance = (TextView) findViewById(R.id.distanceTv);
        mTimeLimit = (TextView) findViewById(R.id.timeLimitTv);
        mMinPlayers = (TextView) findViewById(R.id.minPlayersTv);
        mMaxPlayers = (TextView) findViewById(R.id.maxPlayersTv);
        mHumanReadableLocation = (TextView) findViewById(R.id.humanReadableLocationTv);
        mCurrentScore = (TextView) findViewById(R.id.currentScoreTv);
        mTasksCompleted = (TextView) findViewById(R.id.tasksCompletedTv);
        mCollectedCollectibles = (TextView) findViewById(R.id.collectedCollectiblesTv);
        mState = (TextView) findViewById(R.id.stateTv);
        mLeave = (Button) findViewById(R.id.leaveGameBtn);
        mSelectTask = (Button) findViewById(R.id.selectTaskBtn);
        mPlayers = (ListView) findViewById(R.id.playersLv);
        mStartGame = (Button) findViewById(R.id.startGameBtn);
        mLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leaveGame();
            }
        });
        mAugust = (ImageView) findViewById(R.id.augustIv);
        mSelectTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GameActivity.this, AssignTaskActivity_.class);
                intent.putExtra("gameInstance", mCurrentGame);
                if (mUsers != null) {
                    intent.putExtra("users", new ArrayList<>(mUsers));
                }
                startActivityForResult(intent, 0);
            }
        });
        mStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentGame.setStartDate(new Date());
                mCurrentGame.setState(GameInstance.STATE_PLAYING);
                renderUi();
                startGame();
            }
        });
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mUserNames);
        mPlayers.setAdapter(mAdapter);
    }

    @UiThread
    public void renderUi() {
        if (mCurrentGame != null && mCurrentGame.getGameDescription() != null) {
            mName.setText(mCurrentGame.getGameDescription().getTitle());
            mDescription.setText(mCurrentGame.getGameDescription().getDescription());
            if (mCurrentGame.getGameDescription().getLatitude() != null && mCurrentGame.getGameDescription().getLongitude() != null && mApp.getLocationAsLatLng() != null) {
                LatLng latLng = new LatLng(mCurrentGame.getGameDescription().getLatitude(), mCurrentGame.getGameDescription().getLongitude());
                double latLngDistance = LocationUtils.getLatLngDistance(mApp.getLocationAsLatLng(), latLng);
                mDistance.setText("Start distance: " + latLngDistance + " m");
            }
            mTimeLimit.setText("Max duration: " + (mCurrentGame.getGameDescription().getTimeLimitSeconds() / 60) + " minutes");
            mMinPlayers.setText(MessageFormat.format("Min players: {0}", mCurrentGame.getGameDescription().getMinPlayers()));
            mMaxPlayers.setText(MessageFormat.format("Max players: {0}", mCurrentGame.getGameDescription().getMaxPlayers()));
            if (mCurrentGame.getGameDescription().getHumanReadableStartLocation() != null) {
                mHumanReadableLocation.setText(mCurrentGame.getGameDescription().getHumanReadableStartLocation());
            }
            mCurrentScore.setText(String.format("Current score: %s", mCurrentGame.getBaseScore()));
            mTasksCompleted.setText(String.format("%d/%d tasks completed", mCurrentGame.getCompletedTasks().size(), mCurrentGame.getGameDescription().getTasks().size()));
            mCollectedCollectibles.setText(String.format("%d/%d collectibles collected (optional)", mCurrentGame.getCollectedCollectibles().size(), mCurrentGame.getGameDescription().getCollectibles().size()));
            mState.setText(mCurrentGame.getStateDescriptionAsString(GameActivity.this));
            if ((mCurrentGame.getState().equals(GameInstance.STATE_READY) && mCurrentGame.getCurrentTasksOfUser(mApp.getCurrentUser().getUsername()).isEmpty()) || (mCurrentGame.getPlayerIds().size() < mCurrentGame.getGameDescription().getTasks().size())) {
                mSelectTask.setVisibility(View.VISIBLE);
            } else {
                mSelectTask.setVisibility(View.GONE);
            }
            if (mCurrentGame.getState().equals(GameInstance.STATE_GATHERED)) {
                mStartGame.setVisibility(View.VISIBLE);
            } else {
                mStartGame.setVisibility(View.GONE);
            }
            Double elapsedTimeMin = mCurrentGame.getElapsedTimeMins();
            if (elapsedTimeMin > 10) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mAugust.setImageDrawable(getDrawable(R.drawable.august_2));
                } else {
                    mAugust.setImageDrawable(getResources().getDrawable(R.drawable.august_2));
                }
            } else if (elapsedTimeMin > 20) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mAugust.setImageDrawable(getDrawable(R.drawable.august_3));
                } else {
                    mAugust.setImageDrawable(getResources().getDrawable(R.drawable.august_3));
                }
            }
        }
    }

    @Background
    public void refetchGameInstance() {
        GameInstance gameInstance = GameService.loadGameInstance(mCurrentGame.getId());
        onGameInstanceFetched(gameInstance);
    }

    @UiThread
    public void onGameInstanceFetched(GameInstance gameInstance) {
        this.mCurrentGame = gameInstance;
        if (mUsers != null) {
            this.mCurrentGame.updateState(mUsers);
        }
        if (mUpdater != null) {
            mUpdater.setGameInstance(mCurrentGame);
        }
        renderUi();
    }

    @Background
    public void refetchUsers() {
        onUsersFetched(UserService.getUsers(mCurrentGame.getPlayerIds()));
    }

    @UiThread
    public void onUsersFetched(Collection<User> players) {
        if (this.mUsers == null) {
            this.mUsers = new ArrayList<>();
        }
        this.mUsers.clear();
        this.mUsers.addAll(players);
        if (this.mUpdater == null) {
            mUpdater = new FunkyGameInstanceUpdater(GameActivity.this, mUsers, mCurrentGame);
            handleMessages(mMessagesToHandle);
        }
        this.mCurrentGame.updateState(mUsers);
        this.mUserNames.clear();
        for (User user : mUsers) {
            mUserNames.add(user.getUsername());
        }
        mAdapter.notifyDataSetChanged();
        renderUi();
    }

    @Override
    public void updateGameInstanceState() {
        super.updateGameInstanceState();
        renderUi();
    }

    @Background
    public void startGame() {
        GameService.saveGameInstance(mCurrentGame);
        notifyGameCreated();
    }

    @UiThread
    public void notifyGameCreated() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("startDate", new Date());
        MessagingService.sendStateUpdatedMessage(mCurrentGame.getId(), mCurrentGame.getState(), map, mApp.getCurrentUser().getId());
    }

    @Background
    public void leaveGame() {
        try {
            UserService.leaveGame(mApp.getCurrentUser().getId(), mCurrentGame.getId());
            onGameLeft(true);
        } catch (BackendlessException e) {
            onGameLeft(false);
        }
    }

    @UiThread
    public void onGameLeft(boolean success) {
        if (!success) {
            showToast("Hey, fucker, you ain't leavin' my game! Sorry, something went wrong.");
        } else {
            showToast("Ok, you've left. What now?");
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gameUpdateScheduler.shutdown();
        userUpdateScheduler.shutdown();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            mCurrentGame = (GameInstance) data.getSerializableExtra("gameInstance");
            refetchGameInstance();
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra("fromGameCreated", false)) {
            Intent intent = new Intent(GameActivity.this, MainActivity_.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }
}
