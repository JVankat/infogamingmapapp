package de.htw_dresden.infogamingmapapp.model;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;

import de.htw_dresden.infogamingmapapp.utils.LocationUtils;
import de.htw_dresden.infogamingmapapp.utils.SecurityUtils;
import weborb.service.ExcludeProperties;

/**
 * Created by vanka on 18.01.2017.
 */

@ExcludeProperties(propertyNames = {"collectibleTypeAsString", "pin"})
public class Collectible implements Serializable {

    private static final Integer LOCATION_TOLERANCE_RADIUS_METER = 50;

    private String id;
    private Double latitude;
    private Double longitude;
    private Double pointsAwarded;

    //TODO: get pin method so it could be rendered to map

    public Collectible() {
        this.id = SecurityUtils.generateRandomStringId();
    }

    public Collectible(LatLng location, Double pointsAwarded) {
        this();
        this.latitude = location.latitude;
        this.longitude = location.longitude;
        this.pointsAwarded = pointsAwarded;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getPointsAwarded() {
        return pointsAwarded;
    }

    public void setPointsAwarded(Double pointsAwarded) {
        this.pointsAwarded = pointsAwarded;
    }


    public MarkerOptions getPin(Context context) {
        if (this.latitude == null || this.longitude == null) {
            return null;
        }
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(this.latitude, this.longitude));
        markerOptions.title("Schatz");
        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), android.R.drawable.star_off));
        markerOptions.icon(bitmapDescriptor);
        return markerOptions;
    }

    public boolean canBeCollected(LatLng playerLocation) {
        return ((this.latitude == null || this.longitude == null) && playerLocation != null) ||
                (playerLocation != null && LocationUtils.getLatLngDistance(new LatLng(this.latitude, this.longitude), playerLocation) <= LOCATION_TOLERANCE_RADIUS_METER);
    }
}
