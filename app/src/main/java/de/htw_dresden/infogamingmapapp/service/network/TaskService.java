package de.htw_dresden.infogamingmapapp.service.network;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.exceptions.BackendlessException;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.model.CompletedTaskToGame;
import de.htw_dresden.infogamingmapapp.model.MultipleChoiceQuestion;
import de.htw_dresden.infogamingmapapp.model.MultipleChoiceQuestionToTask;
import de.htw_dresden.infogamingmapapp.model.TaskToGame;
import de.htw_dresden.infogamingmapapp.model.task.MultipleChoiceTask;
import de.htw_dresden.infogamingmapapp.model.task.Task;

/**
 * Created by vanka on 10.01.2017.
 */

public class TaskService {

    private TaskService() {

    }

    public static void save(MultipleChoiceTask mc) throws BackendlessException {
        Backendless.Persistence.of(MultipleChoiceTask.class).save(mc);
        for (MultipleChoiceQuestion question : mc.getQuestions()) {
            saveMultipleChoiceQuestion(question);
            assignQuestionToMultipleChoiceTask(question.getId(), mc.getId());
        }
    }

    private static void assignQuestionToMultipleChoiceTask(String questionId, String taskId) {
        MultipleChoiceQuestionToTask mcqtt = new MultipleChoiceQuestionToTask(questionId, taskId);
        Backendless.Persistence.of(MultipleChoiceQuestionToTask.class).save(mcqtt);
    }

    public static void saveMultipleChoiceQuestion(MultipleChoiceQuestion mcq) throws BackendlessException {
        Backendless.Persistence.of(MultipleChoiceQuestion.class).save(mcq);
    }

    public static Collection<Task> getTasks() throws BackendlessException {

        Collection<Task> tasks = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);

        BackendlessCollection<MultipleChoiceTask> data = Backendless.Data.of(MultipleChoiceTask.class).find(query);
        Collection<MultipleChoiceQuestionToTask> questionsOfMultipleChoice = getQuestionsOfMultipleChoice();
        Collection<MultipleChoiceQuestion> multipleChoiceQuestions = getMultipleChoiceQuestions();

        while (data.getCurrentPage().size() > 0) {
            for (MultipleChoiceTask task : data.getCurrentPage()) {
                List<MultipleChoiceQuestion> questions = new ArrayList<>();
                for (MultipleChoiceQuestionToTask assignment : questionsOfMultipleChoice) {
                    if (assignment.getTaskId().equals(task.getId())) {
                        for (MultipleChoiceQuestion multipleChoiceQuestion : multipleChoiceQuestions) {
                            if (multipleChoiceQuestion.getId().equals(assignment.getQuestionId())) {
                                questions.add(multipleChoiceQuestion);
                            }
                        }
                    }
                }
                task.setQuestions(questions);
                tasks.add(task);
            }
            data = data.nextPage();
        }
        return tasks;
    }

    public static Collection<MultipleChoiceQuestionToTask> getQuestionsOfMultipleChoice() {
        Collection<MultipleChoiceQuestionToTask> assignments = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);

        BackendlessCollection<MultipleChoiceQuestionToTask> data = Backendless.Data.of(MultipleChoiceQuestionToTask.class).find(query);

        while (data.getCurrentPage().size() > 0) {
            for (MultipleChoiceQuestionToTask question : data.getCurrentPage()) {
                assignments.add(question);
            }
            data = data.nextPage();
        }
        return assignments;
    }

    public static Collection<MultipleChoiceQuestion> getMultipleChoiceQuestions() {
        Collection<MultipleChoiceQuestion> questions = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);

        BackendlessCollection<MultipleChoiceQuestion> data = Backendless.Data.of(MultipleChoiceQuestion.class).find(query);

        while (data.getCurrentPage().size() > 0) {
            for (MultipleChoiceQuestion question : data.getCurrentPage()) {
                questions.add(question);
            }
            data = data.nextPage();
        }
        return questions;
    }

    public static void assignTasksToGame(String gameId, Collection<Task> tasks) throws BackendlessException {
        Set<String> taskIds = new HashSet<>();
        for (Task task : tasks) {
            taskIds.add(task.getId());
        }
        assignTaskIdsToGame(gameId, taskIds);
    }

    public static void assignTaskIdsToGame(String gameId, Collection<String> taskIds) throws BackendlessException {
        for (String taskId : taskIds) {
            assignTaskToGame(gameId, taskId);
        }
    }

    public static void assignTaskToGame(String gameId, String taskId) throws BackendlessException {
        TaskToGame gameToTask = new TaskToGame(taskId, gameId);
        Backendless.Persistence.of(TaskToGame.class).save(gameToTask);
    }

    public static Collection<TaskToGame> getTaskGameAssignments() {
        return getTaskGameAssignments(null);
    }

    public static Collection<TaskToGame> getTaskGameAssignments(String queryString) {
        Collection<TaskToGame> assignments = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        BackendlessCollection<TaskToGame> data = Backendless.Data.of(TaskToGame.class).find(query);

        while (data.getCurrentPage().size() > 0) {
            for (TaskToGame assignment : data.getCurrentPage()) {
                assignments.add(assignment);
            }
            data = data.nextPage();
        }
        return assignments;
    }

    public static Collection<Task> getTasksOfGame(String gameDescriptionId) {
        Collection<Task> tasks = getTasks();
        Collection<Task> tasksOfGame = new ArrayList<>();
        Collection<TaskToGame> taskGameAssignments = getTaskGameAssignments("gameId = '" + gameDescriptionId + "'");
        for (TaskToGame taskToGame : taskGameAssignments) {
            if (taskToGame.getGameId().equals(gameDescriptionId)) {
                for (Task task : tasks) {
                    if (task.getId() != null && task.getId().equals(taskToGame.getTaskId())) {
                        tasksOfGame.add(task);
                    }
                }
            }
        }
        return tasksOfGame;

    }

    public static void markTaskAsCompleted(String taskId, String gameId, Double points) throws BackendlessException {
        Backendless.Persistence.of(CompletedTaskToGame.class).save(new CompletedTaskToGame(taskId, gameId, points));
    }

    public static Collection<CompletedTaskToGame> loadCompletedTasksOfGames() {
        return loadCompletedTasksOfGames(null);
    }

    private static Collection<CompletedTaskToGame> loadCompletedTasksOfGames(String queryString) {
        Collection<CompletedTaskToGame> assignments = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        BackendlessCollection<CompletedTaskToGame> data = Backendless.Data.of(CompletedTaskToGame.class).find(query);

        while (data.getCurrentPage().size() > 0) {
            for (CompletedTaskToGame assignment : data.getCurrentPage()) {
                assignments.add(assignment);
            }
            data = data.nextPage();
        }
        return assignments;
    }

    public static Collection<CompletedTaskToGame> loadCompletedTasksOfGame(String gameId) {
        List<CompletedTaskToGame> result = new ArrayList<>();
        for (CompletedTaskToGame completedTaskToGame : loadCompletedTasksOfGames("gameId = '" + gameId + "'")) {
            if (completedTaskToGame.getGameId().equals(gameId)) {
                result.add(completedTaskToGame);
            }
        }
        return result;
    }
}
