package de.htw_dresden.infogamingmapapp.service.network;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.exceptions.BackendlessException;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.model.CollectedCollectibleToGame;
import de.htw_dresden.infogamingmapapp.model.Collectible;
import de.htw_dresden.infogamingmapapp.model.CollectibleToGame;

/**
 * Created by vanka on 20.01.2017.
 */

public class CollectibleService {

    public static void save(Collectible collectible) throws BackendlessException {
        Backendless.Persistence.of(Collectible.class).save(collectible);
    }

    public static Collection<Collectible> load() throws BackendlessException {
        return load(null);
    }

    public static Collection<Collectible> load(String queryString) {
        Collection<Collectible> collectibles = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        BackendlessCollection<Collectible> data = Backendless.Data.of(Collectible.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (Collectible collectible : data.getCurrentPage()) {
                collectibles.add(collectible);
            }
            data = data.nextPage();
        }
        return collectibles;
    }

    public static void assignCollectiblesToGame(String gameId, List<Collectible> collectibles) throws BackendlessException {
        Set<String> collectibleIds = new HashSet<>();
        for (Collectible collectible : collectibles) {
            collectibleIds.add(collectible.getId());
        }
        assignCollectibleIdsToGame(gameId, collectibleIds);
    }

    public static void assignCollectibleIdsToGame(String gameId, Collection<String> collectibleIds) throws BackendlessException {
        for (String id : collectibleIds) {
            assignCollectibleToGame(gameId, id);
        }
    }

    public static void assignCollectibleToGame(String gameId, String collectibleId) throws BackendlessException {
        CollectibleToGame collectibleToGame = new CollectibleToGame(gameId, collectibleId);
        Backendless.Persistence.of(CollectibleToGame.class).save(collectibleToGame);
    }

    public static Collection<CollectibleToGame> getCollectibleGameAssignments() {
        return getCollectibleGameAssignments(null);
    }

    public static Collection<CollectibleToGame> getCollectibleGameAssignments(String queryString) {
        Collection<CollectibleToGame> collectiblesToGames = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        query.setPageSize(80);
        BackendlessCollection<CollectibleToGame> data = Backendless.Data.of(CollectibleToGame.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (CollectibleToGame collectible : data.getCurrentPage()) {
                collectiblesToGames.add(collectible);
            }
            data = data.nextPage();
        }
        return collectiblesToGames;
    }

    public static Collection<Collectible> getCollectiblesOfGame(String gameDescriptionId) throws BackendlessException {
        Collection<Collectible> collectibles = load();
        Collection<CollectibleToGame> collectibleGameAssignments = getCollectibleGameAssignments("gameId = '" + gameDescriptionId + "'");
        return getCollectiblesOfGame(gameDescriptionId, collectibles, collectibleGameAssignments);
    }

    public static Collection<Collectible> getCollectiblesOfGame(String gameDescriptionId, Collection<Collectible> collectibles, Collection<CollectibleToGame> collectibleGameAssignments) {
        List<Collectible> collectiblesOfGame = new ArrayList<>();
        for (CollectibleToGame collectibleToGame : collectibleGameAssignments) {
            if (!collectibleToGame.getGameId().equals(gameDescriptionId)) {
                continue;
            }
            for (Collectible collectible : collectibles) {
                if (collectible.getId().equals(collectibleToGame.getCollectibleId())) {
                    collectiblesOfGame.add(collectible);
                }
            }
        }
        return collectiblesOfGame;
    }

    public static void markCollectibleAsCollected(String collectibleId, String gameId) throws BackendlessException {
        Backendless.Persistence.of(CollectedCollectibleToGame.class).save(new CollectedCollectibleToGame(collectibleId, gameId));
    }

    public static Collection<CollectedCollectibleToGame> loadCollectedCollectibles() {
        return loadCollectedCollectibles(null);
    }

    public static Collection<CollectedCollectibleToGame> loadCollectedCollectibles(String queryString) {
        List<CollectedCollectibleToGame> collectedCollectiblesToGames = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        query.setPageSize(80);
        BackendlessCollection<CollectedCollectibleToGame> data = Backendless.Data.of(CollectedCollectibleToGame.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (CollectedCollectibleToGame collectible : data.getCurrentPage()) {
                collectedCollectiblesToGames.add(collectible);
            }
            data = data.nextPage();
        }
        return collectedCollectiblesToGames;
    }


    public static Collection<String> getCollectedCollectiblesOfGame(String gameId) {
        Collection<CollectedCollectibleToGame> collectedCollectibleToGames = loadCollectedCollectibles("gameId = '" + gameId + "'");
        Set<String> result = new HashSet<>();
        for(CollectedCollectibleToGame collectibleToGame: collectedCollectibleToGames){
            if(collectibleToGame.getGameId().equals(gameId)){
                result.add(collectibleToGame.getCollectibleId());
            }
        }
        return result;
    }
}
