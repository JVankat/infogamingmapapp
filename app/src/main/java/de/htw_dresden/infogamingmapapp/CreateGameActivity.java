package de.htw_dresden.infogamingmapapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import java.util.List;

import de.htw_dresden.infogamingmapapp.adapter.GameDescriptionAdapter;
import de.htw_dresden.infogamingmapapp.model.GameDescription;
import de.htw_dresden.infogamingmapapp.model.GameInstance;
import de.htw_dresden.infogamingmapapp.model.Team;
import de.htw_dresden.infogamingmapapp.service.network.GameService;

@EActivity
public class CreateGameActivity extends BaseActivity {

    List<GameDescription> mGameDescriptions;
    Team mTeam;
    ListView mListView;
    GameDescriptionAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);
        mGameDescriptions = (List<GameDescription>) getIntent().getSerializableExtra("gameDescriptions");
        mTeam = (Team) getIntent().getSerializableExtra("team");
        mListView = (ListView) findViewById(R.id.selectGameLv);

        mAdapter = new GameDescriptionAdapter(CreateGameActivity.this, R.layout.game_description_item, mGameDescriptions, mApp.getLocationAsLatLng());
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GameDescription selectedGameDescription = mAdapter.getItem(i);
                if (selectedGameDescription == null) {
                    return;
                }
                showToast("You selected " + selectedGameDescription.getTitle());
                GameInstance gameInstance = new GameInstance(selectedGameDescription.getId(), mTeam.getId());
                gameInstance.joinGame(mApp.getCurrentUser().getId());
                gameInstance.setGameDescription(selectedGameDescription);
                saveGameInstance(gameInstance);
            }
        });
    }

    @Background
    public void saveGameInstance(GameInstance gameInstance) {
        GameService.saveGameInstance(gameInstance);
        onGameInstanceSaved(gameInstance);
    }

    @UiThread
    public void onGameInstanceSaved(GameInstance gameInstance) {
        Intent intent = new Intent(CreateGameActivity.this, GameActivity_.class);
        intent.putExtra("gameInstance", gameInstance);
        intent.putExtra("fromGameCreated", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
