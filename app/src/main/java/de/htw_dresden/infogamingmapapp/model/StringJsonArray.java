package de.htw_dresden.infogamingmapapp.model;

import org.json.JSONArray;

import java.io.Serializable;

import weborb.service.ExcludeProperty;

/**
 * Created by vanka on 16.01.2017.
 */

@ExcludeProperty(propertyName = "stringValue")
public class StringJsonArray extends JSONArray implements Serializable {

    String value;

    //TODO: I guess we should parse the value when retrieving from backend in setValue
    public String getValue(){
        return this.toString();
    }

    public String getStringValue() {
        return this.value;
    }

}
