package de.htw_dresden.infogamingmapapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessException;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.messaging.Message;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.htw_dresden.infogamingmapapp.model.Collectible;
import de.htw_dresden.infogamingmapapp.model.GameInstance;
import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.service.MessagingService;
import de.htw_dresden.infogamingmapapp.service.network.CollectibleService;
import de.htw_dresden.infogamingmapapp.service.network.TaskService;
import de.htw_dresden.infogamingmapapp.service.network.UserService;

/**
 * Base InfoBlindMapApp Activity. Should include all callbacks (e.g. when position is changed),... that should occur in all activities.
 */
@EActivity
public abstract class BaseActivity extends AppCompatActivity {

    protected App mApp;

    protected Integer PERMISSION_LOCATION = 1;
    private static final Integer GAME_STATE_UPDATE_PERIOD_SECONDS = 10;
    private static final Integer GAME_STATE_UPDATE_OFFSET_SECONDS = 0;

    private static final Integer INTENT_SHOW_TASK = 5428;

    protected Set<String> mSubscribedChannels = new HashSet<>();

    protected FunkyGameInstanceUpdater mUpdater;
    protected GameInstance mCurrentGame;
    protected List<User> mUsers = new ArrayList<>();
    ScheduledExecutorService stateUpdateScheduler = Executors.newSingleThreadScheduledExecutor();
    private boolean isTaskBeingShown = false;


    protected void updateUpdater() {
        if (mCurrentGame == null) {
            return;
        }
        if (mUpdater == null) {
            mUpdater = new FunkyGameInstanceUpdater(BaseActivity.this, mUsers, mCurrentGame);
        }
        mUpdater.setGameInstance(mCurrentGame);
        subscribeToChannel();
    }

    @UiThread
    public void updateGameInstanceState() {
        updateUpdater();
        if (mUpdater != null && mCurrentGame != null) {
            mUpdater.updateGameState();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = ((App) getApplication());
        if (mApp.registerLocationListener()) {
            Log.i(App.TAG, "Location enabled");
        } else {
            Log.i(App.TAG, "Requesting location permission");
            ActivityCompat.requestPermissions(BaseActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION);
        }
        stateUpdateScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                updateGameInstanceState();
                checkForAvailableTasks();
                collectCollectibles();
            }
        }, GAME_STATE_UPDATE_OFFSET_SECONDS, GAME_STATE_UPDATE_PERIOD_SECONDS, TimeUnit.SECONDS);
    }

    @UiThread
    public void collectCollectibles() {
        if (mCurrentGame != null && mCurrentGame.getState().equals(GameInstance.STATE_PLAYING)) {
            for (Collectible collectible : mCurrentGame.getGameDescription().getCollectibles()) {
                if (!mCurrentGame.getCollectedCollectibles().contains(collectible.getId()) && collectible.canBeCollected(mApp.getLocationAsLatLng())) {
                    mCurrentGame.markCollectibleAsCollected(collectible.getId());
                    saveCollectedCollectible(mCurrentGame.getId(), collectible.getId());
                    showToast("You've collected a treasure and received " + collectible.getPointsAwarded() + " points.");
                }
            }
        }
    }

    @UiThread
    public void checkForAvailableTasks() {
        if (mCurrentGame != null && mCurrentGame.getState().equals(GameInstance.STATE_PLAYING)) {
            if (isTaskBeingShown) {
                return;
            }
            for (Task task : mCurrentGame.getCurrentTasksOfUser(mApp.getCurrentUser().getId())) {
                if (task.isTaskAvailable(mApp.getLocationAsLatLng())) {
                    Intent intent = new Intent(BaseActivity.this, TaskActivity_.class);
                    intent.putExtra("task", task);
                    mApp.sendTaskAvailableNotification(task);
                    isTaskBeingShown = true;
                    startActivityForResult(intent, INTENT_SHOW_TASK);
                    break;
                }
            }
        }
    }

    @Background
    public void saveCollectedCollectible(String gameId, String collectibleId) {
        CollectibleService.markCollectibleAsCollected(collectibleId, gameId);
        notifyCollectedCollectible(gameId, collectibleId);
    }

    public void notifyCollectedCollectible(String gameId, String collectibleId) {
        MessagingService.sendCollectibleCollectedMessage(gameId, collectibleId, mApp.getCurrentUser().getId());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_LOCATION) {
            if (grantResults.length == 0) {
                Log.i(App.TAG, "Location permission not granted");
                mApp.setLocationDisabled();
            } else {
                Log.i(App.TAG, "Location permission granted");
                mApp.locationEnabled = true;
                mApp.registerLocationListener();
            }
        }
    }

    @UiThread
    public void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Explorer, you failed!").setMessage(message);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.create().show();
    }

    @UiThread
    public void showToast(String message) {
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Background
    public void logout() {
        try {
            UserService.logout(BaseActivity.this);
        } catch (BackendlessException e) {
            e.printStackTrace();
            Backendless.UserService.setCurrentUser(null);
        }
        mApp.setCurrentUser(null);
        onLoggedOut();
    }

    @UiThread
    public void onLoggedOut() {
        onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @UiThread
    public void subscribeToChannel() {
        if (mCurrentGame == null || mSubscribedChannels.contains(mCurrentGame.getId())) {
            return;
        }
        String channelId = mCurrentGame.getId();
        mSubscribedChannels.add(channelId);

        Backendless.Messaging.subscribe(channelId, new AsyncCallback<List<Message>>() {
            @Override
            public void handleResponse(List<Message> response) {
                handleMessages(response);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        }, new AsyncCallback<Subscription>() {
            @Override
            public void handleResponse(Subscription response) {
                Log.i(App.TAG, "Subscribed to " + response.getChannelName());
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.i(App.TAG, "FUCKING BACKENDLESS ERROR: " + fault.getDetail());
            }
        });
    }

    @UiThread
    public void handleMessages(List<Message> messages) {
        if (mUpdater == null || mCurrentGame == null) {
            return;
        }
        for (Message message : messages) {
            try {
                JSONObject data = new JSONObject(message.getData().toString());
                mUpdater.digestMessage(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        try {
            messages.clear();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }
    }

    @Background
    public void saveCompletedTask(String gameId, String taskId, Double points) {
        TaskService.markTaskAsCompleted(taskId, gameId, points);
        notifyCompletedTask(gameId, taskId, points);
    }

    @UiThread
    public void notifyCompletedTask(String gameId, String taskId, Double points) {
        MessagingService.sendTaskCompletedMessage(taskId, gameId, points, mApp.getCurrentUser().getId());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_SHOW_TASK) {
            isTaskBeingShown = false;
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == INTENT_SHOW_TASK) {
                Double points = data.getDoubleExtra("points", 0d);
                String taskId = data.getStringExtra("taskId");
                mCurrentGame.markTaskAsCompleted(taskId, points);
                saveCompletedTask(mCurrentGame.getId(), taskId, points);
            }
        }
    }
}
