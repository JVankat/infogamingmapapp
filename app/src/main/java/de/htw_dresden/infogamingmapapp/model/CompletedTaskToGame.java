package de.htw_dresden.infogamingmapapp.model;

/**
 * Created by vanka on 28.01.2017.
 */

public class CompletedTaskToGame {

    private String gameId;
    private String taskId;
    private Double points;

    public CompletedTaskToGame() {

    }

    public CompletedTaskToGame(String taskId, String gameId, Double points) {
        this.gameId = gameId;
        this.taskId = taskId;
        this.points = points;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }
}
