package de.htw_dresden.infogamingmapapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.EActivity;

import java.util.ArrayList;
import java.util.List;

import de.htw_dresden.infogamingmapapp.model.GameDescription;
import de.htw_dresden.infogamingmapapp.model.Team;

@EActivity
public class SelectTeamActivity extends BaseActivity {

    ArrayList<GameDescription> mGameDescriptions;
    List<Team> mTeams;
    List<String> mTeamNames;

    ListView mListView;
    TextView mLabelTv;
    Button mCreateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_team);
        mGameDescriptions = (ArrayList<GameDescription>) getIntent().getSerializableExtra("gameDescriptions");
        mTeams = (List<Team>) getIntent().getSerializableExtra("teams");
        mListView = (ListView) findViewById(R.id.selectTeamLv);
        mLabelTv = (TextView) findViewById(R.id.selectTeamTv);
        mCreateBtn = (Button) findViewById(R.id.createTeamBtn);
        if (!mTeams.isEmpty()) {
            mTeamNames = new ArrayList<>();
            for (Team team : mTeams) {
                mTeamNames.add(team.getName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mTeamNames);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Team selectedTeam = mTeams.get(i);
                    showToast("You've selected " + selectedTeam.getName());
                    Intent intent = new Intent(SelectTeamActivity.this, CreateGameActivity_.class);
                    intent.putExtra("team", selectedTeam);
                    intent.putExtra("gameDescriptions", mGameDescriptions);
                    startActivity(intent);
                }
            });
        } else {
            mListView.setVisibility(View.GONE);
            mLabelTv.setVisibility(View.GONE);
            mCreateBtn.setVisibility(View.VISIBLE);
            mCreateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SelectTeamActivity.this, CreateTeamActivity_.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        }
    }
}
