package de.htw_dresden.infogamingmapapp.model;

import com.backendless.BackendlessUser;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;

/**
 * Created by vanka on 07.12.2016.
 */

public class User implements Serializable {

    private String id;
    private String username;
    private Double latitude;
    private Double longitude;

    public User(BackendlessUser bUser) {
        this.id = bUser.getUserId();
        this.username = (String) bUser.getProperty("name");
        Double latitude = bUser.getProperty("latitude") != null ? Double.valueOf(bUser.getProperty("latitude").toString()) : null;
        Double longitude = bUser.getProperty("longitude") != null ? Double.valueOf(bUser.getProperty("longitude").toString()) : null;
        if (latitude != null && longitude != null) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LatLng getLocation() {
        if (this.latitude == null || this.longitude == null) {
            return null;
        }
        return new LatLng(this.latitude, this.longitude);
    }

    public void setLocation(LatLng location) {
        this.latitude = location.latitude;
        this.longitude = location.longitude;
    }

    public void setLocation(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public MarkerOptions getPin() {
        if (this.getLocation() == null) {
            return null;
        }
        MarkerOptions pin = new MarkerOptions();
        pin.position(this.getLocation());
        pin.title(this.getUsername());
        return pin;
    }
}
