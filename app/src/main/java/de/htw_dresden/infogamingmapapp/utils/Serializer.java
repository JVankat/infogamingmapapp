package de.htw_dresden.infogamingmapapp.utils;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by vanka on 19.01.2017.
 */

/**
 * Serializer saving serialized object in private directory of app. Thus filePaths must not contain sub directories.
 */
public class Serializer {

    Context mContext;

    private static final String DEFAULT_EXTENSION = "bin";
    private static final String BOOT_PREFIX = "boot_";

    public Serializer(Context context) {
        mContext = context;
    }

    /**
     * Serialize boot object
     *
     * @param serializable the serializable to serialize
     * @param fileName     the name of the file to save to, without extension, extension will be .bin
     */
    public void serializeBoot(Object serializable, String fileName) throws IOException {
        serializeBoot(serializable, fileName, DEFAULT_EXTENSION);
    }

    /**
     * Serialize boot object
     *
     * @param serializable  the serializable to serialize
     * @param fileName      the name of the file to save to, without extension
     * @param fileExtension file extension without leading '.'
     */
    public void serializeBoot(Object serializable, String fileName, String fileExtension) throws IOException {
        String filePath = BOOT_PREFIX + fileName + "." + fileExtension;
        serialize(serializable, filePath);
    }

    /**
     * Serialize object
     *
     * @param serializable the serializable to serialize
     * @param filePath     the path including file name
     */
    public void serialize(Object serializable, String filePath) throws IOException {
        FileOutputStream fos = mContext.openFileOutput(filePath, Context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(serializable);
        oos.close();
        fos.close();
    }

    /**
     * Deserialize boot object
     *
     * @param fileName the file name, where serialized object should be stored, resulting path will be data/boot/[fileName}.bin
     */
    public Object deserializeBoot(String fileName) throws IOException, ClassNotFoundException {
        return deserializeBoot(fileName, DEFAULT_EXTENSION);
    }

    /**
     * Deserialize boot object, resulting path will be boot_{fileName}.{fileExtension}
     *
     * @param fileName      the file name, where serialized object should be stored
     * @param fileExtension the file extension without leading '.'
     */
    public Object deserializeBoot(String fileName, String fileExtension) throws IOException, ClassNotFoundException {
        String filePath = BOOT_PREFIX + fileName + "." + fileExtension;
        return deserialize(filePath);
    }

    public Object deserialize(String filePath) throws IOException, ClassNotFoundException {
        FileInputStream fis = mContext.openFileInput(filePath);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object object = ois.readObject();
        ois.close();
        fis.close();
        return object;
    }

}
