package de.htw_dresden.infogamingmapapp.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;

/**
 * Created by vanka on 10.01.2017.
 */

public class ViewUtils {
    public static int getColor(Context context, int id) {
        return ContextCompat.getColor(context, id);
    }
}
