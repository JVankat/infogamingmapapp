package de.htw_dresden.infogamingmapapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.model.task.Task;

/**
 * Created by vanka on 19.12.2016.
 */

public class TaskAdapter extends ArrayAdapter<Task> {

    List<Task> mTask;

    public TaskAdapter(Context context, int resource, List<Task> objects) {
        super(context, resource, objects);
        mTask = objects;
    }

    @Nullable
    @Override
    public Task getItem(int position) {
        return mTask.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.task_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.nameView = (TextView) convertView.findViewById(R.id.taskName);
            viewHolder.descriptionView = (TextView) convertView.findViewById(R.id.taskDescription);
            viewHolder.ownerView = (TextView) convertView.findViewById(R.id.taskOwner);
            viewHolder.listView = (LinearLayout) convertView.findViewById(R.id.taskItemll);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Task currentTask = getItem(position);
        if (currentTask != null) {
            viewHolder.nameView.setText(currentTask.getName());
//            viewHolder.descriptionView.setText(currentTask.getDescription());
            viewHolder.ownerView.setText(currentTask.getOwner());
            String owner = currentTask.getOwner();
            if (owner == null) {
                viewHolder.listView.setBackgroundColor(Color.WHITE);
                viewHolder.nameView.setTextColor(Color.BLACK);
            } else {
                viewHolder.listView.setBackgroundColor(Color.GREEN);
                viewHolder.nameView.setTextColor(Color.WHITE);
            }
        }
        return convertView;
    }

    public void setOwner(String owner, int position) {
        mTask.get(position).setOwner(owner);
    }

    private static class ViewHolder {
        private TextView nameView;
        private TextView descriptionView;
        private TextView ownerView;
        private LinearLayout listView;
    }

}

