package de.htw_dresden.infogamingmapapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.backendless.exceptions.BackendlessException;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.utils.NetworkUtils;
import de.htw_dresden.infogamingmapapp.utils.SharedPreferenceHelper;
import de.htw_dresden.infogamingmapapp.utils.ValidationUtils;

import static de.htw_dresden.infogamingmapapp.utils.ValidationUtils.validateNotEmpty;

@EActivity
public class LoginActivity extends BaseActivity {

    @ViewById
    EditText emailEt;
    @ViewById
    EditText passwordEt;
    @ViewById
    Button loginBtn;
    @ViewById
    Button registerBtn;
    private boolean disableApiCalls = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Click(R.id.loginBtn)
    public void onLoginClick() {
        if(!NetworkUtils.isConnectedToNetwork(LoginActivity.this)) {
            showToast("Explorer. Please connect to network.");
        }else{
            login(emailEt.getText().toString(), passwordEt.getText().toString());
        }
    }

    @Click(R.id.registerBtn)
    public void onRegisterClick() {
        de.htw_dresden.infogamingmapapp.RegistrationActivity_.intent(this).start();
    }

    @Background
    public void login(String email, String password) {
        if (!validateLogin(email, password)) {
            alertInvalidLogin();
        } else {
            try {
                if (!disableApiCalls) {
                    disableApiCalls = true;
                    onLoggedIn(de.htw_dresden.infogamingmapapp.service.network.UserService.login(email, password), email, password);
                }
            } catch (BackendlessException e) {
                showDialog(e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
        disableApiCalls = false;
    }

    private boolean validateLogin(String email, String password) {
        return validateNotEmpty(email, password) && ValidationUtils.isValidEmail(email);
    }

    @UiThread
    public void onLoggedIn(User user, String email, String password) {
        Log.i(App.TAG, user.getUsername() + " logged in");
        SharedPreferenceHelper.saveEmail(this, email);
        SharedPreferenceHelper.savePassword(this, password);
        Intent intent = new Intent(LoginActivity.this, MainActivity_.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @UiThread
    public void alertInvalidLogin() {
        if (!validateNotEmpty(emailEt.getText().toString(), passwordEt.getText().toString())) {
            String emptyFields = "";
            boolean emptyUsername = false;
            if (emailEt.getText().toString().isEmpty()) {
                emailEt.requestFocus();
                emptyUsername = true;
                emptyFields += "Email";
            }
            if (passwordEt.getText().toString().isEmpty()) {
                if (emptyUsername) {
                    emptyFields += " and ";
                } else {
                    passwordEt.requestFocus();
                }
                emptyFields += "Password";
            }
            emptyFields += " " + "is empty. Try again, failure!";
            showDialog(emptyFields);
        } else {
            showDialog("Invalid email format. Try again, failure!");
        }

    }

}
