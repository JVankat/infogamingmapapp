package de.htw_dresden.infogamingmapapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.model.Team;

/**
 * Created by vanka on 19.12.2016.
 */

public class TeamAdapter extends ArrayAdapter<Team> {

    List<Team> mTeam;

    public TeamAdapter(Context context, int resource, List<Team> objects) {
        super(context, resource, objects);
        mTeam = objects;
    }

    @Nullable
    @Override
    public Team getItem(int position) {
        return mTeam.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.team_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.nameView = (TextView) convertView.findViewById(R.id.teamName);
            viewHolder.privateView = (TextView) convertView.findViewById(R.id.teamPrivate);
            viewHolder.memberCount = (TextView) convertView.findViewById(R.id.memberCountTv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Team team = getItem(position);
        if (team != null) {
            String teamName = team.getName();
            if (team.joined) {
                teamName += " (joined)";
            }
            viewHolder.nameView.setText(teamName);
            if(team.memberIds!=null){
                viewHolder.memberCount.setText(team.memberIds.size()+"/"+team.getMemberThreshold()+" members");
            }
            if (!team.isPublic()) {
                viewHolder.privateView.setVisibility(View.VISIBLE);
            }
        }

        return convertView;
    }


    private static class ViewHolder {
        private TextView nameView;
        private TextView privateView;
        private TextView memberCount;
    }

}
