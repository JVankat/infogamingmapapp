package de.htw_dresden.infogamingmapapp.model.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.widget.LinearLayout;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;

import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.utils.LocationUtils;
import de.htw_dresden.infogamingmapapp.utils.SecurityUtils;
import weborb.service.ExcludeProperties;

/**
 * Created by vanka on 09.01.2017.
 */
@ExcludeProperties(propertyNames = {"pin", "owner", "GLASS", "GOLD", "CONSTRUCTION_PLAN", "WOOD", "SAND_STONE", "drawableId"})
public abstract class Task implements Serializable {


    public static final int GLASS = 0;
    public static final int GOLD = 1;
    public static final int CONSTRUCTION_PLAN = 2;
    public static final int WOOD = 3;
    public static final int SAND_STONE = 4;

    protected static final Integer LOCATION_TOLERANCE_RADIUS = 50;

    protected String id;
    protected TaskInterface listener;
    protected Double latitude, longitude;
    protected String description = "";
    protected String name = "";
    protected String owner = null;
    protected Integer taskType = GLASS;

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    public Task() {
        this.id = SecurityUtils.generateRandomStringId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Render task into view. onClick Listeners,... should be initialized at this point.
     *
     * @param context    the current context
     * @param parentView the view where task should be rendered, must be <code>LinearLayout</code>
     */
    public abstract void append(Context context, LinearLayout parentView);

    protected void onTaskCompleted(Double pointsReceiver) {
        listener.onTaskCompleted(pointsReceiver);
    }

    public void setListener(TaskInterface mListener) {
        this.listener = mListener;
    }

    public boolean isTaskAvailable(LatLng userPosition) {
        return userPosition != null && (this.latitude == null || this.longitude == null || LocationUtils.getLatLngDistance(userPosition, new LatLng(latitude, longitude)) <= (double) LOCATION_TOLERANCE_RADIUS);
    }

    public void setLocation(LatLng location) {
        this.latitude = location.latitude;
        this.longitude = location.longitude;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public MarkerOptions getPin(Context context, boolean isTaskOfCurrentUser) {
        if (this.latitude == null || this.longitude == null) {
            return null;
        }
        MarkerOptions pin = new MarkerOptions();
        pin.position(new LatLng(this.latitude, this.longitude));
        Integer drawableId = getDrawableId();
        if (drawableId != null) {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), drawableId);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 60, 60, false);
            if (!isTaskOfCurrentUser) {
                BitmapDrawable bmpDrawable = new BitmapDrawable(context.getResources(), scaledBitmap);
                bmpDrawable.setAlpha(122);
                scaledBitmap = bmpDrawable.getBitmap();
            }
            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(scaledBitmap);
            pin.icon(bitmapDescriptor);
        }
        pin.title(getName());
        return pin;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    private Integer getDrawableId() {
        switch (this.getTaskType()) {
            case GLASS:
                return R.drawable.glass;
            case GOLD:
                return R.drawable.gold;
            case CONSTRUCTION_PLAN:
                return R.drawable.construction_plan;
            case SAND_STONE:
                return R.drawable.sand_stone;
            case WOOD:
                return R.drawable.wood;
        }
        return null;
    }
}
