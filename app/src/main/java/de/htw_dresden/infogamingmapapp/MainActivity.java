package de.htw_dresden.infogamingmapapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.backendless.Backendless;
import com.backendless.exceptions.BackendlessException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.htw_dresden.infogamingmapapp.fragment.TeamFragment;
import de.htw_dresden.infogamingmapapp.model.Collectible;
import de.htw_dresden.infogamingmapapp.model.GameDescription;
import de.htw_dresden.infogamingmapapp.model.GameInstance;
import de.htw_dresden.infogamingmapapp.model.Team;
import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.model.UserToTeam;
import de.htw_dresden.infogamingmapapp.model.task.MultipleChoiceTask;
import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.service.MessagingService;
import de.htw_dresden.infogamingmapapp.service.network.GameService;
import de.htw_dresden.infogamingmapapp.service.network.TaskService;
import de.htw_dresden.infogamingmapapp.service.network.TeamService;
import de.htw_dresden.infogamingmapapp.service.network.UserService;
import de.htw_dresden.infogamingmapapp.utils.NetworkUtils;
import de.htw_dresden.infogamingmapapp.utils.Serializer;
import de.htw_dresden.infogamingmapapp.utils.SharedPreferenceHelper;

@SuppressWarnings("unchecked")
@EActivity
public class MainActivity extends BaseActivity implements TeamFragment.OnFragmentInteractionListener {

    private static final int INTENT_TASK = 0;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private GoogleMap map;
    List<Marker> mCurrentMarkers = new ArrayList<>();
    private boolean mHasMapBeenFocused = false;

    /**
     * Cache all users
     */
    private Collection<User> mUsers = new ArrayList<User>();
    /**
     * Ids of current user's teams
     */
    private Set<String> mMyTeams = new HashSet<>();
    /**
     * All games available (description of those games)
     */
    private List<GameDescription> mGameDescriptions = new ArrayList<>();


    private GameInstance mCurrentGame = null;

    private List<Team> mTeams = new ArrayList<>();
    private List<Task> mTasks = new ArrayList<>();
    private List<GameInstance> mGameInstances = new ArrayList<>();
    private boolean isDialogBeingShown = false;
    private boolean hasStopped = false;
    private Set<String> mIgnoredGames = new HashSet<>();
    private boolean isActivityRunning = true;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    ScheduledExecutorService lowPrioScheduler = Executors.newSingleThreadScheduledExecutor();
    ScheduledExecutorService midPrioScheduler = Executors.newSingleThreadScheduledExecutor();
    ScheduledExecutorService highPrioScheduler = Executors.newSingleThreadScheduledExecutor();

    private static final Integer LOW_PRIO_REFRESH_RATE_SECONDS = 120;
    private static final Integer MID_PRIO_REFRESH_RATE_SECONDS = 60;
    private static final Integer HIGH_PRIO_REFRESH_RATE_SECONDS = 30;
    private static final Integer REFRESH_START_DELAY_SECONDS = 5;

    private Button mStateButton;
    private ImageView mZwinger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        if (mSectionsPagerAdapter == null) {
            Log.i(App.TAG, "PagedAdapter is null!");
        }
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        mStateButton = (Button) findViewById(R.id.gameStateBtn);
        mZwinger = (ImageView) findViewById(R.id.zwinger);

        updateStateButton();

        deserializeEverything();
        if (mGameDescriptions.isEmpty()) {
            mStateButton.setVisibility(View.GONE);
        }
        initializeSchedulers();
        //  createGame();
    }

    @UiThread
    public void updateStateButton() {
        mStateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentGame == null || mCurrentGame.getState().equals(GameInstance.STATE_FINISHED) || !mCurrentGame.getPlayerIds().contains(mApp.getCurrentUser().getId())) {
                    Intent intent = new Intent(MainActivity.this, SelectTeamActivity_.class);
                    intent.putExtra("gameDescriptions", new ArrayList<>(mGameDescriptions));
                    ArrayList<Team> myTeams = new ArrayList<Team>();
                    for (Team team : mTeams) {
                        if (mMyTeams != null && mMyTeams.contains(team.getId())) {
                            myTeams.add(team);
                        }
                    }
                    intent.putExtra("teams", myTeams);
                    startActivity(intent);
                } else if (mCurrentGame != null) {
                    Intent intent = new Intent(MainActivity.this, GameActivity_.class);
                    intent.putExtra("gameInstance", mCurrentGame);
                    intent.putExtra("users", new ArrayList<>(mUsers));
                    startActivity(intent);
                }
            }
        });
    }

    private void initializeSchedulers() {
        lowPrioScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                fetchLowPriorityData();
            }
        }, REFRESH_START_DELAY_SECONDS, LOW_PRIO_REFRESH_RATE_SECONDS, TimeUnit.SECONDS);
        midPrioScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                fetchMidPriorityData();
            }
        }, REFRESH_START_DELAY_SECONDS, MID_PRIO_REFRESH_RATE_SECONDS, TimeUnit.SECONDS);

        highPrioScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                fetchHighPriorityData();
                serializeEverything();
            }
        }, REFRESH_START_DELAY_SECONDS, HIGH_PRIO_REFRESH_RATE_SECONDS, TimeUnit.SECONDS);
    }


    public void fetchLowPriorityData() {
        if (mApp.isLoggedIn()) {
            //fetching users when game is being played happens on another thread
            if (mCurrentGame == null || mCurrentGame.getState().equals(GameInstance.STATE_FINISHED)) {
                fetchAllUsers();
            }
            fetchMyTeams();
            fetchGameDescriptions();
        }
    }

    @Background
    public void fetchAllUsers() {
        Log.i(App.TAG, "Fetching all users");
        Collection<User> users = UserService.getAllUSers();
        Log.i(App.TAG, "Fetched " + users.size() + " users");
        onUsersFetched(users);
    }

    @UiThread
    public void onUsersFetched(Collection<User> users) {
        mUsers.clear();
        mUsers.addAll(users);
    }

    @Background
    public void fetchMyTeams() {
        onMyTeamsFetched(TeamService.getMyTeamIds(mApp.getCurrentUser().getId()));
    }

    @UiThread
    public void onMyTeamsFetched(Collection<String> myTeamIds) {
        mMyTeams.clear();
        mMyTeams.addAll(myTeamIds);
        for (Team team : mTeams) {
            boolean joined = false;
            for (String myTeamId : mMyTeams) {
                if (myTeamId.equals(team.getId())) {
                    joined = true;
                    Log.i(App.TAG, "I am member of team " + team.getName());
                    break;
                }
            }
            team.joined = joined;
        }
        onTeamsFetched(null);
    }

    //TODO: FIXME THIS IS FOCKIN UGLY
    @UiThread
    public void onTeamsFetched(Collection<Team> teams) {
        if (teams != null) {
            Log.i(App.TAG, "Fetched " + teams.size() + " teams");
            mTeams.clear();
            ;
            mTeams.addAll(teams);
        }
        if (mViewPager != null && !hasStopped) {
            mViewPager.setAdapter(mSectionsPagerAdapter); //hacky way to force to redraw all tabs //FIXME: find better way to redraw only the team fragment
        }
        //       mSectionsPagerAdapter.notifyDataSetChanged();
        //  mSectionsPagerAdapter.getItem(1);
        // if (mSectionsPagerAdapter.getItem(1) != null) {
        //     ((TeamFragment) mSectionsPagerAdapter.getItem(1)).notifyTeamsUpdated();
        // }
    }

    @Background
    public void fetchGameDescriptions() {
        Collection<GameDescription> gameDescriptions = null;
        gameDescriptions = GameService.loadGameDescriptions();
        onGameDescriptionsFetched(gameDescriptions);
    }

    @UiThread
    public void onGameDescriptionsFetched(Collection<GameDescription> gameDescriptions) {
        if (gameDescriptions != null) {
            mGameDescriptions.clear();
            mGameDescriptions.addAll(gameDescriptions);
            Log.i(App.TAG, "Fetched " + gameDescriptions.size() + " game descriptions");
            if (!mGameDescriptions.isEmpty()) {
                mStateButton.setVisibility(View.VISIBLE);
            }
            if (gameDescriptions.isEmpty()) {
                createGame();
            }
        }
    }


    @Background
    public void createGame() {
        GameDescription.Builder builder = new GameDescription.Builder();
        builder.setDescription("Willkommen an meinem Hofe! Mein Name ist August der Starke und ich möchte die StadtDresden in neuem Glanze erstrahlen lassen. Zuerst soll mit Eurer Hilfe der Zwinger errichtetwerden, ein Schloss, in dem bald die Hochzeit meines Sohnes stattfinden wird. Beginnt schnell und begebt Euch in die Mitte des Zwingers. Dort erfahrt Ihr den nächsten Schritt.");
        List<Task> tasks = new ArrayList<>();
        List<Collectible> collectibles = new ArrayList<>();
        MultipleChoiceTask task1 = new MultipleChoiceTask();
        task1.setName("Begebt Euch in die Frauenkirche. Dort erhaltet Ihr Sandstein.");
        task1.setDescription("Ihr seid angekommen. Beantwortet folgende Frage, um Sandstein zu erhalten:");
        task1.setLocation(new LatLng(51.051915, 13.741560));
        List<String> answers = new ArrayList<>();
        answers.add("1B");
        answers.add("2C");
        answers.add("3D");
        answers.add("4E");
        task1.addQuestion("Welche Nummerierung steht auf der letzten Bankreihe im Mittelgang?", answers, "3D");
        task1.setTaskType(Task.SAND_STONE);
        task1.setPointsThreshold(500d);
        tasks.add(task1);
        MultipleChoiceTask task2 = new MultipleChoiceTask();
        task2.setName("Begebt Euch in die Hofkirche. Dort erhaltet Ihr Holz.");
        task2.setDescription("Ihr seid angekommen. Beantwortet folgende Frage, um Holz zu erhalten:");
        task2.setLocation(new LatLng(51.0535124, 13.7351701));
        answers = new ArrayList<>();
        answers.add("Buch");
        answers.add("Kerzenständer");
        answers.add("Grabmal");
        answers.add("Kreuz");
        task2.addQuestion("Was befindet sich unter dem Triptychon im vom Eingang aus gesehen vorderen rechten Teilder Kirche?", answers, "Buch");
        task2.setTaskType(Task.WOOD);
        task2.setPointsThreshold(500d);
        tasks.add(task2);
        MultipleChoiceTask task3 = new MultipleChoiceTask();
        task3.setName("Bleibt im Zwinger. Hier erhaltet Ihr Baupläne.");
        task3.setDescription("Ihr seid angekommen. Beantwortet folgende Frage, um Baupläne zu erhalten:");
        task3.setLocation(new LatLng(51.053015, 13.733758));
        answers = new ArrayList<>();
        answers.add("Vögel");
        answers.add("Fische");
        answers.add("Rehe");
        answers.add("Hunde");
        task3.addQuestion("Welches Tiere halten die kindlichen Steinfiguren im Arm, die sich auf den Balkonen zumInnenhof Richtung Semperoper befinden?", answers, "Fische");
        task3.setTaskType(Task.CONSTRUCTION_PLAN);
        task3.setPointsThreshold(500d);
        tasks.add(task3);
        MultipleChoiceTask task4 = new MultipleChoiceTask();
        task4.setName("Begebt euch zum Residenzschloss. Hier erhaltet Ihr Glas.");
        task4.setDescription("Ihr seid angekommen. Beantwortet folgende Frage, um Glas zu erhalten:");
        task4.setLocation(new LatLng(51.0525009, 13.7344717));
        answers = new ArrayList<>();
        answers.add("1");
        answers.add("2");
        answers.add("3");
        answers.add("4");
        task4.addQuestion("Wie viele Reihen hat die Perlenkette der Büste von Carola, Königin v. Sachsen?", answers, "4");
        task4.setTaskType(Task.GLASS);
        task4.setPointsThreshold(500d);
        tasks.add(task4);
        MultipleChoiceTask task5 = new MultipleChoiceTask();
        task5.setName("Begebt euch zum Fürstenzug. Hier erhaltet Ihr Kunstwerke.");
        task5.setDescription("Ihr seid angekommen. Beantwortet folgende Frage, um Kunstwerke zu erhalten:");
        task5.setLocation(new LatLng(51.0528113, 13.7366263));
        task5.addQuestion("Wie viele Windhunde sind auf dem Fürstenzug zu sehen?", answers, "2");
        task5.setTaskType(Task.GOLD);
        task5.setPointsThreshold(500d);
        tasks.add(task5);
        builder.setHumanReadableStartLocation("Zwinger");
        builder.setLocation(new LatLng(51.053015, 13.733758));
        builder.setTimeLimitSeconds((long) (45 * 60));
        builder.setMinPlayers(1);
        builder.setMaxPlayers(5);
        Collectible c = new Collectible(new LatLng(51.053933, 13.735705), 100.0);
        collectibles.add(c);
        c = new Collectible(new LatLng(51.0490289, 13.7354262), 100d);
        collectibles.add(c);
        builder.setTasks(tasks);
        builder.setCollectibles(collectibles);
        builder.setTitle("Zwingerbau");
        GameDescription gameDescription = builder.create();
        GameService.saveGameDescription(gameDescription);
    }

    private void fetchMidPriorityData() {
        if (mApp.isLoggedIn()) {
            fetchAllTeams();
        }
    }

    @Background
    public void fetchAllTeams() {
        Collection<Team> teams = TeamService.getTeams();
        onTeamsFetched(teams);
    }


    private void fetchHighPriorityData() {
        if (mApp.isLoggedIn()) {
            fetchGameInstances();
            updateCurrentLocation();
            if (mCurrentGame != null && !mCurrentGame.getState().equals(GameInstance.STATE_FINISHED)) {
                fetchAllUsers();
            }
        }
    }

    @Background
    public void fetchGameInstances() {
        Collection<GameInstance> gameInstances = GameService.loadGameInstances();
        onGameInstancesFetched(gameInstances);
    }

    @UiThread
    public void onGameInstancesFetched(Collection<GameInstance> gameInstances) {
        Log.i(App.TAG, "Fetched " + gameInstances.size() + " game instances");
        mGameInstances.clear();
        mGameInstances.addAll(gameInstances);
        for (GameInstance gameInstance : mGameInstances) {
            if (gameInstance.getPlayerIds().contains(mApp.getCurrentUser().getId()) || mMyTeams.contains(gameInstance.getTeamId()) && !gameInstance.getState().equals(GameInstance.STATE_FINISHED)) {
                boolean subscribe = false;
                if ((mCurrentGame == null || !mCurrentGame.getId().equals(gameInstance.getId())) && (gameInstance.getPlayerIds().contains(mApp.getCurrentUser().getId()))) {
                    subscribe = true;
                }
                mCurrentGame = gameInstance;
                if (subscribe) {
                    subscribeToChannel();
                }
                break;
            }
        }
        updateGameState();
    }

    @UiThread
    public void updateGameState() {
        if (!isActivityRunning) {
            return;
        }
        if (mCurrentGame != null && mCurrentGame.getGameDescription() != null && !mCurrentGame.getState().equals(GameInstance.STATE_FINISHED) && mCurrentGame.getPlayerIds().contains(mApp.getCurrentUser().getId())) {
            boolean stateChanged = mCurrentGame.updateState(new ArrayList<User>(mUsers));
            if (stateChanged) {
                updateStateButton();
                saveGameInstance(mCurrentGame);
            }
            mStateButton.setText(mCurrentGame.getGameDescription().getTitle() + ": " + mCurrentGame.getStateDescriptionAsString(MainActivity.this));
        } else {
            if (mCurrentGame != null && mCurrentGame.getGameDescription() != null && !mCurrentGame.getState().equals(GameInstance.STATE_FINISHED) && !mIgnoredGames.contains(mCurrentGame.getId())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                Team gameTeam = new Team("unknown");
                for (Team team : mTeams) {
                    if (team.getId().equals(mCurrentGame.getTeamId())) {
                        gameTeam = team;
                        break;
                    }
                }
                builder.setTitle("Join game?").setMessage("Do you want to join game " + mCurrentGame.getGameDescription().getTitle() + " (team " + gameTeam.getName() + ")?");
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        joinGame(mCurrentGame.getId());
                        MessagingService.sendUserJoinedGameMessage(mApp.getCurrentUser().getId(), mCurrentGame.getId());

                    }
                });
                builder.setNegativeButton(android.R.string.no, null);
                mIgnoredGames.add(mCurrentGame.getId());
                builder.create().show();
            }
            mStateButton.setText(getString(R.string.label_create_game));
        }
    }

    @Background
    public void joinGame(String gameId) {
        UserService.joinGame(mApp.getCurrentUser().getId(), gameId);
        onGameJoined(gameId);
    }

    @UiThread
    public void onGameJoined(String gameId) {
        mCurrentGame.joinGame(mApp.getCurrentUser().getId());
        updateStateButton();
        updateGameState();
    }

    @Background
    public void saveGameInstance(GameInstance gameInstance) {
        saveGameInstance(gameInstance);
    }

    @UiThread
    public void showTask(final Task task) {
        if (!isDialogBeingShown) {
            isDialogBeingShown = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Do you wanna play, lil' boy?");
            builder.setMessage(task.getName() + " is available");
            builder.setNegativeButton("Nope, nope, nope!", null);
            //TODO: vibration to notify that task is available
            builder.setPositiveButton("Yeah!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(MainActivity.this, TaskActivity.class);
                    intent.putExtra("task", task);
                    startActivityForResult(intent, INTENT_TASK);
                }
            });
            builder.create().show();
        }
    }

    private void focusMap() {
        if (map != null && mApp.getLocation() != null) {
            Log.i(App.TAG, "Focusing user location");
            LatLng latLng = mApp.getLocationAsLatLng();

            CameraPosition camPos = new CameraPosition.Builder()
                    .target(latLng)
                    .zoom(13)
                    .bearing(mApp.getLocation().getBearing())
                    .tilt(70)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
            mHasMapBeenFocused = true;
        }
    }

    @Background
    public void updateCurrentLocation() {
        Log.i(App.TAG, "Updating my location");
        UserService.updateUserLocation(mApp.getCurrentUser(), mApp.getLocationAsLatLng());
    }

    @Override
    @Background
    public void joinTeam(String teamId) {
        try {
            TeamService.joinTeam(mApp.getCurrentUser().getId(), teamId);
        } catch (Exception e) {
            e.printStackTrace();
            showDialog("Sorry, we failed.");
        }
        onTeamJoined();
    }

    @Override
    public boolean isMemberOfTeam(String teamId) {
        for (String myTeamId : mMyTeams) {
            if (myTeamId.equals(teamId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Team> getTeams() {
        return this.mTeams;
    }

    @Background
    public void getTasks(String teamId) {
        try {
            TaskService.getTasks();
        } catch (Exception e) {
            e.printStackTrace();
            showDialog("Sorry, we failed.");
        }
        onTeamJoined();
    }

    @UiThread
    public void onTeamJoined() {
        fetchAllTeams();
        fetchMyTeams();
    }

    @UiThread
    public void serializeEverything() {
        long serializationStart = System.currentTimeMillis();
        Serializer serializer = new Serializer(MainActivity.this);
        serializeBootObject(serializer, mGameDescriptions, "list_" + GameDescription.class.getName());
        serializeBootObject(serializer, mUsers, "list_" + User.class.getName());
        serializeBootObject(serializer, mTeams, "list_" + Team.class.getName());
        serializeBootObject(serializer, mMyTeams, "list_" + UserToTeam.class.getName());
        serializeBootObject(serializer, mTasks, "list_" + Task.class.getName());
        serializeBootObject(serializer, mGameInstances, "list_" + GameInstance.class.getName());
        try {
            if (mCurrentGame != null) {
                mCurrentGame.serializationTimeStamp = new Date();
                serializer.serializeBoot(mCurrentGame, GameInstance.class.getName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(App.TAG, "Serialization of all data took " + (System.currentTimeMillis() - serializationStart) + " ms");
    }

    @UiThread
    public void deserializeEverything() {
        Serializer serializer = new Serializer(MainActivity.this);
        try {
            mGameDescriptions = (List<GameDescription>) deserializeBootObjectOrReturnEmptyList(serializer, "list_" + GameDescription.class.getName());

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        try {
            mUsers = (Collection<User>) deserializeBootObjectOrReturnEmptyList(serializer, "list_" + User.class.getName());
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        try {
            mTeams = (List<Team>) deserializeBootObjectOrReturnEmptyList(serializer, "list_" + Team.class.getName());
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        try {
            mMyTeams = (HashSet<String>) deserializeBootObjectOrReturnEmptyList(serializer, "list_" + UserToTeam.class.getName());

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        try {
            mTasks = (List<Task>) deserializeBootObjectOrReturnEmptyList(serializer, "list_" + Task.class.getName());
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        try {
            mGameInstances = (List<GameInstance>) deserializeBootObjectOrReturnEmptyList(serializer, "list_" + GameInstance.class.getName());
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        try {
            GameInstance gameInstance = (GameInstance) serializer.deserializeBoot(GameInstance.class.getName());
            //deserialize game instance only in case it was serialized less than five minutes ago
            if (gameInstance != null && gameInstance.serializationTimeStamp != null && 1000 * 60 * 5 <= ((new Date().getTime()) - gameInstance.serializationTimeStamp.getTime())) {
                mCurrentGame = gameInstance;
            }
        } catch (IOException | ClassCastException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        Log.i(App.TAG, "Deserialized " + mGameDescriptions.size() + " game descriptions");
        Log.i(App.TAG, "Deserialized " + mUsers.size() + " users");
        Log.i(App.TAG, "Deserialized " + mTeams.size() + " teams");
        Log.i(App.TAG, "Deserialized " + mMyTeams.size() + " my team assignments");
        Log.i(App.TAG, "Deserialized " + mTasks.size() + " tasks");
        Log.i(App.TAG, "Deserialized " + mGameInstances.size() + " game instances");
    }

    private Object deserializeBootObjectOrReturnEmptyList(Serializer serializer, String className) {
        Object object = deserializeBootObject(serializer, className);
        if (object == null) {
            return new ArrayList<>();
        }
        return object;
    }

    private Object deserializeBootObject(Serializer serializer, String className) {
        try {
            return serializer.deserializeBoot(className);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void serializeBootObject(Serializer serializer, Object object, String className) {
        try {
            serializer.serializeBoot(object, className);
            Log.i(App.TAG, className + " was serialized");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Task> getTasks() {
        return mTasks;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter implements OnMapReadyCallback {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position == 0) {
                SupportMapFragment mapFragment = SupportMapFragment.newInstance();
                mapFragment.getMapAsync(this);
                return mapFragment;//MapViewFragment.newInstance(mApp, mUsers);
            }
            if (position == 1) {
                return TeamFragment.newInstance(mApp, mTeams);
            }
            return null;
        }


        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "MAP";
                case 1:
                    return "TEAMS";
            }
            return null;
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            Log.i(App.TAG, "Map ready");
            int i = 1;

            map = googleMap;
            if (mApp.isLocationEnabled()) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Log.i(App.TAG, "Location not enabled");
                    map.setMyLocationEnabled(true);
                }
            }
            if (!mHasMapBeenFocused) {
                focusMap();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityRunning = true;
        validateUserIsLoggedIn();
        hasStopped = false;

    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityRunning = false;
    }

    @Background
    public void validateUserIsLoggedIn() {
        boolean loggedIn = Backendless.UserService.CurrentUser() != null;
        if (!loggedIn && NetworkUtils.isConnectedToNetwork(MainActivity.this)) {
            if (SharedPreferenceHelper.getEmail(this) != null) {
                try {
                    User user = UserService.login(SharedPreferenceHelper.getEmail(this), SharedPreferenceHelper.getPassword(this));
                    Log.i(App.TAG, "User " + user.getUsername() + " was automatically logged in");
                    loggedIn = true;
                } catch (BackendlessException e) {
                    e.printStackTrace();
                }
            }
        }
        if (!loggedIn) {
            Intent intent = new Intent(MainActivity.this, LoginActivity_.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            mApp.setCurrentUser(new User(Backendless.UserService.CurrentUser()));
        }
    }

    @Override
    public void updateGameInstanceState() {
        super.updateGameInstanceState();
        if (mCurrentGame != null && mApp.isLoggedIn() && mCurrentGame.getPlayerIds().contains(mApp.getCurrentUser().getId())) {
            if (map != null) {
                List<MarkerOptions> pins = mCurrentGame.getPins(MainActivity.this, new ArrayList<User>(mUsers), mApp.getCurrentUser().getId());
                for (Marker marker : mCurrentMarkers) {
                    marker.remove();
                }
                mCurrentMarkers.clear();
                for (MarkerOptions pin : pins) {
                    Marker marker = map.addMarker(pin);
                    mCurrentMarkers.add(marker);
                }
            }
            mStateButton.setText(mCurrentGame.getGameDescription().getTitle() + ": " + mCurrentGame.getStateDescriptionAsString(MainActivity.this));
            updateStateButton();
            updateZwinger();
        }
    }

    @UiThread
    public void updateZwinger() {
        if (mCurrentGame != null) {
            Double completed = ((double) mCurrentGame.getCompletedTasks().size()) / ((double) mCurrentGame.getGameDescription().getTasks().size());
            mZwinger.setAlpha(Float.valueOf(completed.toString()));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(App.TAG, "Some activity finished with result");
        switch (requestCode) {
            case 0:
                isDialogBeingShown = false;
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasStopped = true;
        lowPrioScheduler.shutdown();
        midPrioScheduler.shutdown();
        highPrioScheduler.shutdown();
    }
}
