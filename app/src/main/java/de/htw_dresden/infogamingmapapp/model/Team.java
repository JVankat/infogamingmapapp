package de.htw_dresden.infogamingmapapp.model;

import java.io.Serializable;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.utils.SecurityUtils;

/**
 * Created by vanka on 12.12.2016.
 */

public class Team implements Serializable {

    private static final Integer DEFAULT_MEMBER_THRESHOLD = 5;

    private String id;
    private String name;
    //public so it should not be attempted to be saved by Backendless
    public boolean joined = false;
    public Set<String> memberIds;
    private String shaEncryptedPassword;
    private Integer memberThreshold = DEFAULT_MEMBER_THRESHOLD;


    public Integer getMemberThreshold() {
        return memberThreshold != null ? memberThreshold : DEFAULT_MEMBER_THRESHOLD;
    }

    public void setMemberThreshold(Integer memberThreshold) {
        this.memberThreshold = memberThreshold;
    }

    public Team() {


    }

    public Team(String name) {
        this(name, null);
    }

    public Team(String name, String password) {
        this.name = name;
        if (password != null) {
            this.shaEncryptedPassword = SecurityUtils.encodeSha1(password);
        }
        this.setId(String.valueOf(name.hashCode()) + SecurityUtils.generateRandomStringId());
    }

    public boolean isPublic() {
        return getShaEncryptedPassword() == null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getShaEncryptedPassword() {
        return shaEncryptedPassword;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setShaEncryptedPassword(String password) {
        this.shaEncryptedPassword = password;
    }
}
