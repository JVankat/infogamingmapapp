package de.htw_dresden.infogamingmapapp.model;

/**
 * Created by vanka on 20.01.2017.
 */

public class CollectibleToGame {

    private String gameId;
    private String collectibleId;

    public CollectibleToGame(){

    }

    public CollectibleToGame(String gameId, String collectibleId){
        this.gameId = gameId;
        this.collectibleId = collectibleId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getCollectibleId() {
        return collectibleId;
    }

    public void setCollectibleId(String collectibleId) {
        this.collectibleId = collectibleId;
    }
}
