package de.htw_dresden.infogamingmapapp.model;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.App;
import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.utils.LocationUtils;
import de.htw_dresden.infogamingmapapp.utils.SecurityUtils;
import weborb.service.ExcludeProperties;

/**
 * Created by vanka on 18.01.2017.
 */

@ExcludeProperties(propertyNames = {"elapsedTimeMins", "serializationTimeStamp", "gameDescription", "baseScore", "completedTasks", "completedTaskIds", "collectedCollectibles", "playerIds", "STATE_CREATED", "STATE_READY", "STATE_ASSIGNED_TASKS", "STATE_PLAYING", "STATE_FINISHED", "taskAssignments", "myTask", "pins"})

public class GameInstance implements Serializable {

    public Date serializationTimeStamp;

    public static final int STATE_CREATED = 0;
    public static final int STATE_READY = 1;
    public static final int STATE_ASSIGNED_TASKS = 2;
    public static final int STATE_GATHERED = 4;
    public static final int STATE_PLAYING = 5;
    public static final int STATE_WAITING_TO_REGROUP = 6;
    public static final int STATE_FINISHED = 7;

    public static final int LOCATION_TOLERANCE_RADIUS_METER = 50;

    /**
     * Backendless object id
     */
    private String objectId;

    private String id;
    private GameDescription gameDescription;
    private String gameDescriptionId;
    private String teamId;
    private Date startDate;
    private Double points;
    private Integer state = STATE_CREATED;
    private Set<String> playerIds = new HashSet<>();

    //contains completed task ids mapped to points received
    private HashMap<String, Double> completedTasks = new HashMap<>();
    private Set<String> collectedCollectibles = new HashSet<>();

    private List<UserToTask> taskAssignments = new ArrayList<>();

    public GameInstance() {
        this.id = SecurityUtils.generateRandomStringId();
        this.objectId = this.id + SecurityUtils.generateRandomStringId();
    }

    public GameInstance(String gameDescriptionId, String teamId) {
        this();
        this.gameDescriptionId = gameDescriptionId;
        this.teamId = teamId;
        this.objectId = gameDescriptionId + teamId;
    }

    //TODO: interface which notifies main activity about finished game (when this.setState(STATE_FINISHED)), at this point we should also assign points
    //TODO: kick out users that haven't been online for x minutes and then remove task assignment and notify that task is not assigned (or when user has left)
    //TODO: backwards state updates (STATE_READY||STATE_ASSIGNED_TASKS||STATE_GATHERED) -> STATE_CREATED if users leaves

    /**
     * Try to update state of the game instance (if possible).
     *
     * @return true if state was changed and instance should be saved to update persistent data, false otherwise
     */
    public boolean updateState(List<User> users) {
        switch (getState()) {
            case STATE_CREATED:
                if (checkIfCanAssignTasks()) {
                    this.setState(STATE_READY);
                    return true;
                }
                break;
            case STATE_READY:
                if (checkIfTasksAreAssigned()) {
                    if (checkIfAllPlayersAreAtStartPosition(users)) {
                        this.setState(STATE_ASSIGNED_TASKS);
                    } else {
                        this.setState(STATE_GATHERED);
                    }
                    return true;
                }
                break;
            case STATE_ASSIGNED_TASKS:
                if (checkIfAllPlayersAreAtStartPosition(users)) {
                    this.setState(STATE_GATHERED);
                    return true;
                }
                break;
            case STATE_GATHERED:
                //empty, game must be started manually
                break;
            case STATE_PLAYING:
                if (checkIfAllTasksAreFinished()) {
                    if (checkIfAllPlayersAreAtStartPosition(users)) {
                        this.setState(STATE_FINISHED);
                    } else {
                        this.setState(STATE_WAITING_TO_REGROUP);
                    }
                    calculateScore();
                    return true;
                }
//                else if (checkIfTimeWentOut()) {
//                    this.setState(STATE_FINISHED);
//                    return true;
//                }
                break;
            case STATE_WAITING_TO_REGROUP:
                if (checkIfAllPlayersAreAtStartPosition(users)) {
                    this.setState(STATE_FINISHED);
                    calculateScore();
                    return true;
                }
                break;
        }
        return false;
    }

    private void calculateScore() {
        Double score = 0d;
        for (Collectible collectible : getGameDescription().getCollectibles()) {
            if (collectedCollectibles.contains(collectible.getId())) {
                score += collectible.getPointsAwarded();
            }
        }
        for (Map.Entry<String, Double> entry : completedTasks.entrySet()) {
            score += entry.getValue();
        }
        double multiplier = 1;
        double timeRemainingSeconds = ((double) getGameDescription().getTimeLimitSeconds()) - (getElapsedTimeMins() * 60d);
        if (timeRemainingSeconds > 0) {
            multiplier += (timeRemainingSeconds / 2000d);
        }
        int missingPlayers = getGameDescription().getMaxPlayers() - getPlayerIds().size();
        double multiplierForMissingPlayers = missingPlayers == 0 ? 0 : ((double) missingPlayers) / 10d;
        multiplier +=multiplierForMissingPlayers;
        this.points = multiplier*score;
    }

    private boolean checkIfTimeWentOut() {
        long currentTime = new Date().getTime();
        if (this.getStartDate() == null) {
            return false;
        }
        long startTime = this.getStartDate().getTime();
        long difInSeconds = (currentTime - startTime) / 1000;
        return this.getGameDescription().getTimeLimitSeconds() <= difInSeconds;
    }

    private boolean checkIfAllTasksAreFinished() {
        if(getGameDescription()==null){
            return false;
        }
        for (Task task : getGameDescription().getTasks()) {
            if (!completedTasks.keySet().contains(task.getId())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check last known users position against start location and check if all users are near of it. If location of user is unknown, let it pass.
     *
     * @param users list of all users
     * @return true if all players are at start position, false otherwise
     */
    private boolean checkIfAllPlayersAreAtStartPosition(List<User> users) {
        int playersValidated = 0;
        for (User user : users) {
            if (this.getPlayerIds().contains(user.getId())) {
                playersValidated++;
                double distance;
                if (user.getLocation() == null) {
                    distance = 0;
                } else {
                    distance = LocationUtils.getLatLngDistance(user.getLocation(), new LatLng(this.gameDescription.getLatitude(), this.gameDescription.getLongitude()));
                }
                boolean passes = distance <= LOCATION_TOLERANCE_RADIUS_METER;
                if (!passes) {
                    return false;
                } else {
                    Log.i(App.TAG, "User " + user.getUsername() + " is ready at start position (distance: " + distance + ")");
                }
            }
            if (playersValidated == this.getPlayerIds().size()) {
                break;
            }
        }
        return true;
    }

    /**
     * Check if all tasks are assigned
     *
     * @return boolean
     */
    private boolean checkIfTasksAreAssigned() {
        for (Task task : getGameDescription().getTasks()) {
            boolean isTaskAssigned = false;
            for (UserToTask userToTask : this.taskAssignments) {
                if (task.getId().equals(userToTask.getTaskId())) {
                    isTaskAssigned = true;
                    break;
                }
            }
            if (!isTaskAssigned) {
                return false;
            }
        }
        return true;
    }

    private boolean checkIfCanAssignTasks() {
        return this.gameDescription.getMinPlayers() <= this.getPlayerIds().size();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GameDescription getGameDescription() {
        return gameDescription;
    }

    public void setGameDescription(GameDescription gameDescription) {
        this.gameDescription = gameDescription;
    }

    public String getGameDescriptionId() {
        return gameDescriptionId;
    }

    public void setGameDescriptionId(String gameDescriptionId) {
        this.gameDescriptionId = gameDescriptionId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public void markTaskAsCompleted(String taskId, Double pointsReceived) {
        this.completedTasks.put(taskId, pointsReceived);
    }

    public void markCollectibleAsCollected(String collectibleId) {
        this.collectedCollectibles.add(collectibleId);
    }

    public void joinGame(String userId) {
        this.playerIds.add(userId);
    }

    public Set<String> getPlayerIds() {
        return playerIds;
    }

    public void setPlayerIds(Set<String> playerIds) {
        this.playerIds = playerIds;
    }

    public void setTaskAssignments(List<UserToTask> taskAssignments) {
        this.taskAssignments = taskAssignments;
    }

    public void addTaskAssignment(UserToTask userToTask) {
        if (this.taskAssignments == null) {
            this.taskAssignments = new ArrayList<>();
        }
        this.taskAssignments.add(userToTask);
    }

    /**
     * Get current task of user.
     *
     * @param userId the user id
     * @return empty list if user has no more tasks to complete, collection of tasks otherwise
     */
    public Collection<Task> getCurrentTasksOfUser(String userId) {
        if (completedTasks == null) {
            completedTasks = new HashMap<>();
        }
        List<Task> tasksOfUser = new ArrayList<>();
        for (UserToTask userToTask : taskAssignments) {
            if (userToTask.getUserId().equals(userId) && !completedTasks.containsKey(userToTask.getTaskId())) {
                for (Task task : gameDescription.getTasks()) {
                    if (task.getId().equals(userToTask.getTaskId())) {
                        tasksOfUser.add(task);
                    }
                }
            }
        }
        return tasksOfUser;
    }

    public Double getBaseScore() {
        Double score = 0d;
        for (Map.Entry<String, Double> entry : completedTasks.entrySet()) {
            score += entry.getValue();
        }
        for (Collectible collectible : gameDescription.getCollectibles()) {
            if (collectedCollectibles.contains(collectible.getId())) {
                score += collectible.getPointsAwarded();
            }
        }
        return score;
    }


    public HashMap<String, Double> getCompletedTasks() {
        return completedTasks;
    }

    public Set<String> getCollectedCollectibles() {
        return collectedCollectibles;
    }

    public String getStateDescriptionAsString(Context context) {
        String position = getGameDescription().getHumanReadableStartLocation() != null ? " - " + getGameDescription().getHumanReadableStartLocation() : "";
        switch (getState()) {
            case STATE_CREATED:
                return String.format(context.getString(R.string.label_game_waiting), getPlayerIds().size(), getGameDescription().getMinPlayers());
            case STATE_READY:
                return "Selecting tasks";
            case STATE_ASSIGNED_TASKS:
                return "Begebt euch zum " + position;
            case STATE_GATHERED:
                return "Game is ready. Start it!";
            case STATE_PLAYING:
                if (startDate != null) {
                    Date currentDate = new Date();
                    long gameRunningSeconds = (currentDate.getTime() - startDate.getTime()) / 1000;
                    long timeRemaining = gameDescription.getTimeLimitSeconds() - gameRunningSeconds;
                    if (timeRemaining <= 0) {
                        return "So lange werde ich ungeduldig!";
                    } else {
                        Double elapsedTimeMins = ((double) (gameDescription.getTimeLimitSeconds() - timeRemaining)) / 60d;
                        if (elapsedTimeMins >= 30) {
                            return "Rasch, mein guter Ruf steht auf dem Spiel!";
                        } else if (elapsedTimeMins >= 20) {
                            return "Ihr müsst schneller vorankommen!";
                        } else if (elapsedTimeMins >= 10) {
                            return "Eilt Euch, der Zwinger muss rechtzeitig zur Hochzeit meines Sohnes fertig sein!";
                        } else {
                            //TODO: more than hour
                            int minutes = (int) Math.floor((double) timeRemaining / (double) 60);
                            int seconds = (int) (timeRemaining % 60);
                            return "Remaining time: " + minutes + ":" + seconds;
                        }
                    }
                }
                return "Just playing.";
            case STATE_WAITING_TO_REGROUP:
                return "Geht nun zurück in die Mitte des " + position + "s, um den Bau abzuschließen.";
            case STATE_FINISHED:
                return "Vielen Dank für Eure Hilfe. Nun steht der Hochzeit meines Sohnes im Zwinger nichts mehrim Wege und die Stadt Dresden ist um ein prachtvolles Bauwerk gewachsen!";
        }
        return "";
    }

    public List<UserToTask> getTaskAssignments() {
        return taskAssignments;
    }

    public List<MarkerOptions> getPins(Context context, List<User> users, String currentUserId) {
        List<MarkerOptions> pins = new ArrayList<>();
        Set<String> currentUserTasks = new HashSet<>();
        for(Task task: getCurrentTasksOfUser(currentUserId)){
            currentUserTasks.add(task.getId());
        }
        if (getState().equals(STATE_PLAYING)) {
            for (Task task : gameDescription.getTasks()) {
                if (!getCompletedTasks().containsKey(task.getId())) {
                    boolean isTaskOfCurrentUser = currentUserTasks.contains(task.getId());
                    MarkerOptions pin = task.getPin(context,isTaskOfCurrentUser);
                    if (pin != null) {
                        pins.add(pin);
                    }
                }
            }
            for (Collectible collectible : getGameDescription().getCollectibles()) {
                if (!getCollectedCollectibles().contains(collectible.getId())) {
                    pins.add(collectible.getPin(context));
                }
            }
        }
        for (User user : users) {
            if (getPlayerIds().contains(user.getId())) {
                MarkerOptions userPin = user.getPin();
                if (userPin == null) {
                    continue;
                }
                if (getCurrentTasksOfUser(user.getId()).isEmpty()) {
                    userPin.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else {
                    userPin.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }
                pins.add(userPin);
            }
        }
        if (this.getState().equals(STATE_ASSIGNED_TASKS) || this.getState().equals(STATE_WAITING_TO_REGROUP)) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(new LatLng(getGameDescription().getLatitude(), gameDescription.getLongitude()));
            markerOptions.title("Here's where you should go right now!");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
            pins.add(markerOptions);
        }
        return pins;
    }

    public void setCollectedCollectibles(Set<String> collectibleIds) {
        this.collectedCollectibles = collectibleIds;
    }

    public void setCompletedTasks(HashMap<String, Double> completedTasks) {
        this.completedTasks = completedTasks;
    }

    public Double getElapsedTimeMins() {
        if(startDate==null){
            return 0d;
        }
        long gameRunningSeconds = (new Date().getTime() - startDate.getTime()) / 1000;
        long timeRemaining = gameDescription.getTimeLimitSeconds() - gameRunningSeconds;
        Double elapsedTimeMins = ((double) (gameDescription.getTimeLimitSeconds() - timeRemaining)) / 60d;
        return elapsedTimeMins;
    }
}
