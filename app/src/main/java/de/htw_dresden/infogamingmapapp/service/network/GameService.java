package de.htw_dresden.infogamingmapapp.service.network;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.exceptions.BackendlessException;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.model.Collectible;
import de.htw_dresden.infogamingmapapp.model.CompletedTaskToGame;
import de.htw_dresden.infogamingmapapp.model.GameDescription;
import de.htw_dresden.infogamingmapapp.model.GameInstance;
import de.htw_dresden.infogamingmapapp.model.UserToTask;
import de.htw_dresden.infogamingmapapp.model.task.MultipleChoiceTask;
import de.htw_dresden.infogamingmapapp.model.task.Task;

/**
 * Created by vanka on 18.01.2017.
 */

public class GameService {

    public static void saveGameDescription(GameDescription gameDescription) throws BackendlessException {
        Backendless.Persistence.of(GameDescription.class).save(gameDescription);
        for (Task task : gameDescription.getTasks()) {
            if (task instanceof MultipleChoiceTask) {
                TaskService.save((MultipleChoiceTask) task);
            }
        }
        for (Collectible collectible : gameDescription.getCollectibles()) {
            CollectibleService.save(collectible);
        }
        TaskService.assignTasksToGame(gameDescription.getId(), gameDescription.getTasks());
        CollectibleService.assignCollectiblesToGame(gameDescription.getId(), gameDescription.getCollectibles());
    }

    public static void saveGameInstance(GameInstance gameInstance) throws BackendlessException {
        Backendless.Persistence.of(GameInstance.class).save(gameInstance);
        for (String playerId : gameInstance.getPlayerIds()) {
            UserService.joinGame(playerId, gameInstance.getId());
        }
    }

    public static Collection<GameDescription> loadGameDescriptions() throws BackendlessException {
        return loadGameDescriptions(null);
    }

    public static Collection<GameDescription> loadGameDescriptions(String queryString) throws BackendlessException {
        Collection<GameDescription> gameDescriptions = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        BackendlessCollection<GameDescription> data = Backendless.Data.of(GameDescription.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (GameDescription gameDescription : data.getCurrentPage()) {
                gameDescription.setTasks(new ArrayList<Task>(TaskService.getTasksOfGame(gameDescription.getId())));
                gameDescription.setCollectibles(new ArrayList<Collectible>(CollectibleService.getCollectiblesOfGame(gameDescription.getId())));
                gameDescriptions.add(gameDescription);
            }
            data = data.nextPage();
        }
        return gameDescriptions;
    }

    public static GameDescription getGameDescriptionOfGame(String gameDescriptionId) {
        return getGameDescriptionOfGame(gameDescriptionId, loadGameDescriptions("id = '" + gameDescriptionId + "'"));
    }

    private static GameDescription getGameDescriptionOfGame(String gameDescriptionId, Collection<GameDescription> gameDescriptions) {
        for (GameDescription gameDescription : gameDescriptions) {
            if (gameDescription.getId().equals(gameDescriptionId)) {
                return gameDescription;
            }
        }
        return null;
    }

    public static GameInstance loadGameInstance(String gameId) {
        Collection<GameInstance> gameInstances = loadGameInstances("id = '" + gameId + "'");
        if (!gameInstances.isEmpty()) {
            return (new ArrayList<>(gameInstances)).get(0);
        }
        return null;
    }

    public static Collection<GameInstance> loadGameInstances() throws BackendlessException {
        return loadGameInstances(null);
    }

    public static Collection<GameInstance> loadGameInstances(String queryString) throws BackendlessException {
        Collection<GameInstance> gameInstances = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        BackendlessCollection<GameInstance> data = Backendless.Data.of(GameInstance.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (GameInstance gameInstance : data.getCurrentPage()) {
                gameInstance.setPlayerIds(new HashSet<String>(UserService.getUsersOfGame(gameInstance.getId())));
                try {
                    gameInstance.setTaskAssignments(new ArrayList<UserToTask>(getTaskToUserAssignmentsOfGame(gameInstance.getId())));
                } catch (BackendlessException e) {
                    e.printStackTrace();
                }
                try {
                    gameInstance.setCollectedCollectibles(new HashSet<String>(CollectibleService.getCollectedCollectiblesOfGame(gameInstance.getId())));
                } catch (BackendlessException e) {
                    e.printStackTrace();
                }
                try {
                    Collection<CompletedTaskToGame> completedTasks = TaskService.loadCompletedTasksOfGame(gameInstance.getId());
                    HashMap<String, Double> completedTasksMap = new HashMap<>();
                    for (CompletedTaskToGame completedTaskToGame : completedTasks) {
                        completedTasksMap.put(completedTaskToGame.getTaskId(), completedTaskToGame.getPoints());
                    }
                    gameInstance.setCompletedTasks(completedTasksMap);
                } catch (BackendlessException e) {
                    e.printStackTrace();
                }
                gameInstance.setGameDescription(getGameDescriptionOfGame(gameInstance.getGameDescriptionId()));
                gameInstances.add(gameInstance);
            }
            data = data.nextPage();
        }
        return gameInstances;
    }

    public static Collection<UserToTask> getTaskToUserAssignmentsOfGame(String gameId) {
        Set<UserToTask> tasksOfGame = new HashSet<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        query.setWhereClause("gameId = '" + gameId + "'");
        BackendlessCollection<UserToTask> data = Backendless.Data.of(UserToTask.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (UserToTask userToTask : data.getCurrentPage()) {
                if (userToTask.getGameId().equals(gameId)) {
                    tasksOfGame.add(userToTask);
                }
            }
            data = data.nextPage();
        }
        return tasksOfGame;
    }


}
