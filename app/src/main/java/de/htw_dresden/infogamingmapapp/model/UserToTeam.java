package de.htw_dresden.infogamingmapapp.model;

import java.io.Serializable;

/**
 * Created by vanka on 03.01.2017.
 */

public class UserToTeam implements Serializable {
    /**
     * Get backendless object id
     * @return object id as string
     */
    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Backendless object id
     */
    public String objectId;
    public String userId;
    public String teamId;

    public UserToTeam() {

    }

    public UserToTeam(String userId, String teamId) {
        this.userId = userId;
        this.teamId = teamId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
}
