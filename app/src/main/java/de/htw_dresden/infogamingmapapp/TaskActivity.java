package de.htw_dresden.infogamingmapapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import org.androidannotations.annotations.EActivity;

import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.model.task.TaskInterface;

@EActivity
public class TaskActivity extends BaseActivity implements TaskInterface {

    Task mTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        mTask = (Task) getIntent().getSerializableExtra("task");
        mTask.setListener(this);
        mTask.append(TaskActivity.this, (LinearLayout) findViewById(R.id.taskView));
    }

    @Override
    public void onTaskCompleted(Double pointsReceived) {
        Log.i(App.TAG, "Task completed, got " + pointsReceived + " points");
        showToast("You scored " + pointsReceived + " points");
        Intent result = new Intent();
        result.putExtra("taskId",mTask.getId());
        result.putExtra("points",pointsReceived);
        setResult(RESULT_OK,result);
        finish();
    }

    @Override
    public void notifyTasksUpdated() {

    }
}
