package de.htw_dresden.infogamingmapapp.model;

/**
 * Created by vanka on 29.01.2017.
 */

public class MultipleChoiceQuestionToTask {

    private String questionId;
    private String taskId;

    public MultipleChoiceQuestionToTask(){

    }

    public MultipleChoiceQuestionToTask(String questionId, String taskId){
        this.questionId = questionId;
        this.taskId = taskId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
