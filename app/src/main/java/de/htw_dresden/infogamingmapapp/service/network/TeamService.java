package de.htw_dresden.infogamingmapapp.service.network;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.exceptions.BackendlessException;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.model.Team;
import de.htw_dresden.infogamingmapapp.model.UserToTeam;

/**
 * Created by vanka on 19.12.2016.
 */

public class TeamService {

    private TeamService() {

    }

    public static Team saveTeam(Team team) throws BackendlessException {
        return Backendless.Persistence.of(Team.class).save(team);
    }

    public static Collection<Team> getTeams() throws BackendlessException {
        Collection<Team> teams = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        BackendlessCollection<Team> data = Backendless.Data.of(Team.class).find(query);
        Collection<UserToTeam> assignements = getAllUserToTeam();
        while (data.getCurrentPage().size() > 0) {
            for (Team team : data.getCurrentPage()) {
                Set<String> teamUserIds = new HashSet<>();
                for (UserToTeam teamUser : getUsersOfTeam(assignements, team.getId())) {
                    teamUserIds.add(teamUser.getUserId());
                }
                team.memberIds = teamUserIds;
                teams.add(team);
            }
            data = data.nextPage();
        }
        return teams;
    }


    public static Collection<UserToTeam> getAllUserToTeam() {
        return getUserToTeam(null);
    }

    public static Collection<UserToTeam> getUserToTeam(String queryString) {
        Collection<UserToTeam> userToTeams = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        BackendlessCollection<UserToTeam> data = Backendless.Data.of(UserToTeam.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (UserToTeam team : data.getCurrentPage()) {
                userToTeams.add(team);
            }
            data = data.nextPage();
        }
        return userToTeams;
    }

    public static Collection<UserToTeam> getUsersOfTeam(String teamId) {
        return getUsersOfTeam(getAllUserToTeam(), teamId);
    }

    public static Collection<UserToTeam> getUsersOfTeam(Collection<UserToTeam> usersToTeam, String teamId) {
        Collection<UserToTeam> teamUsers = new ArrayList<>();
        for (UserToTeam userToTeam : usersToTeam) {
            if (userToTeam.getTeamId().equals(teamId)) {
                teamUsers.add(userToTeam);
            }
        }
        return teamUsers;
    }

    public static Collection<String> getMyTeamIds(String userId) throws BackendlessException {
        Set<String> teamIds = new HashSet<>();
        for (UserToTeam userToTeam : getMyTeams(userId)) {
            teamIds.add(userToTeam.getTeamId());
        }
        return teamIds;
    }

    public static Collection<UserToTeam> getMyTeams(String userId) throws BackendlessException {
        return getMyTeams(getUserToTeam("userId = '" + userId+"'"), userId);
    }


    public static Collection<UserToTeam> getMyTeams(Collection<UserToTeam> allUserToTeam, String userId) {
        Collection<UserToTeam> myTeams = new ArrayList<>();
        for (UserToTeam userToTeam : allUserToTeam) {
            if (userToTeam.getUserId().equals(userId)) {
                myTeams.add(userToTeam);
            }
        }
        return myTeams;
    }

    public static void joinTeam(String userId, String teamId) throws BackendlessException {
        UserToTeam userToTeam = new UserToTeam(userId, teamId);
        Backendless.Persistence.of(UserToTeam.class).save(userToTeam);
    }

    public static void leaveTeam(String userId, String teamId) throws BackendlessException {
        for (UserToTeam myTeam : getMyTeams(userId)) {
            if (myTeam.getTeamId().equals(teamId)) {
                Backendless.Persistence.of(UserToTeam.class).remove(myTeam);
            }
        }
    }

}
