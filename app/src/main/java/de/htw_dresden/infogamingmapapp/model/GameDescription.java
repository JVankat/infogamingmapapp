package de.htw_dresden.infogamingmapapp.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.utils.SecurityUtils;
import weborb.service.ExcludeProperties;

/**
 * Created by vanka on 18.01.2017.
 */

/**
 * Represents a description of single game (its title, description, location, tasks, collectibles and time limit).
 */
@ExcludeProperties(propertyNames = {"tasks", "collectibles"})
public class GameDescription implements Serializable {

    private static final Integer DEFAULT_MIN_PLAYERS = 1;
    private static final Integer DEFAULT_MAX_PLAYERS = 5;

    private String id;
    private String title;
    private String description;
    private Double latitude;
    private Double longitude;
    private String humanReadableStartLocation;
    private Long timeLimitSeconds;
    private Integer minPlayers = DEFAULT_MIN_PLAYERS;
    private Integer maxPlayers = DEFAULT_MAX_PLAYERS;

    public long getTimeLimitSeconds() {
        return timeLimitSeconds;
    }

    public void setTimeLimitSeconds(long timeLimitSeconds) {
        this.timeLimitSeconds = timeLimitSeconds;
    }

    private List<Task> tasks;
    private List<Collectible> collectibles;

    public GameDescription() {
        this.id = SecurityUtils.generateRandomStringId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<Collectible> getCollectibles() {
        return collectibles;
    }

    public void setCollectibles(List<Collectible> collectibles) {
        this.collectibles = collectibles;
    }

    public void setTimeLimitSeconds(Long timeLimitSeconds) {
        this.timeLimitSeconds = timeLimitSeconds;
    }

    public Integer getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(Integer minPlayers) {
        this.minPlayers = minPlayers;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getHumanReadableStartLocation() {
        return humanReadableStartLocation;
    }

    public void setHumanReadableStartLocation(String humanReadableStartLocation) {
        this.humanReadableStartLocation = humanReadableStartLocation;
    }

    public static class Builder {

        private String title;
        private String description;
        private Double latitude;
        private Double longitude;
        private List<Task> tasks = new ArrayList<>();
        private List<Collectible> collectibles = new ArrayList<>();
        private Long timeLimitSeconds;
        private Integer minPlayers = DEFAULT_MIN_PLAYERS;
        private Integer maxPlayers = DEFAULT_MAX_PLAYERS;
        private String humanReadableStartLocation;

        public Builder() {

        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setLocation(LatLng location) {
            this.latitude = location.latitude;
            this.longitude = location.longitude;
            return this;
        }

        public Builder setTasks(List<Task> tasks) {
            this.tasks = tasks;
            return this;
        }

        public Builder addTask(Task task) {
            this.tasks.add(task);
            return this;
        }

        public Builder setCollectibles(List<Collectible> collectibles) {
            this.collectibles = collectibles;
            return this;
        }

        public Builder addCollectible(Collectible collectible) {
            this.collectibles.add(collectible);
            return this;
        }

        public Builder setMinPlayers(Integer minPlayers) {
            this.minPlayers = minPlayers;
            return this;
        }

        public Builder setMaxPlayers(Integer maxPlayers) {
            this.maxPlayers = maxPlayers;
            return this;
        }

        public Builder setTimeLimitSeconds(Long timeLimitSeconds) {
            this.timeLimitSeconds = timeLimitSeconds;
            return this;
        }

        public Builder setHumanReadableStartLocation(String humanReadableStartLocation) {
            this.humanReadableStartLocation = humanReadableStartLocation;
            return this;
        }

        public GameDescription create() {
            GameDescription gameDescription = new GameDescription();
            gameDescription.title = this.title;
            gameDescription.description = this.description;
            gameDescription.latitude = this.latitude;
            gameDescription.longitude = this.longitude;
            gameDescription.tasks = this.tasks;
            gameDescription.collectibles = this.collectibles;
            gameDescription.timeLimitSeconds = this.timeLimitSeconds;
            gameDescription.minPlayers = this.minPlayers;
            gameDescription.maxPlayers = this.maxPlayers;
            gameDescription.humanReadableStartLocation = this.humanReadableStartLocation;
            return gameDescription;
        }

    }

}
