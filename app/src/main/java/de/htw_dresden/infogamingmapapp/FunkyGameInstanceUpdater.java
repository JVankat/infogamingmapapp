package de.htw_dresden.infogamingmapapp;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.htw_dresden.infogamingmapapp.model.GameInstance;
import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.model.UserToTask;
import de.htw_dresden.infogamingmapapp.service.MessagingService;

/**
 * Created by vanka on 24.01.2017.
 */

public class FunkyGameInstanceUpdater {

    private List<User> mUsers;
    private BaseActivity mContext;
    private GameInstance mGameInstance;

    public FunkyGameInstanceUpdater(BaseActivity context, List<User> users, GameInstance gameInstance) {
        this.mContext = context;
        this.mUsers = users;
        if (this.mUsers == null) {
            this.mUsers = new ArrayList<>();
        }
        mGameInstance = gameInstance;
    }

    public void setGameInstance(GameInstance gameInstance) {
        this.mGameInstance = gameInstance;
    }

    public void digestMessage(JSONObject message) throws JSONException {
        if (message.getString("gameId") == null || !message.getString("gameId").equals(mGameInstance.getId())) {
            return;
        }
        String type = message.getString("type");
        if (type.equals(MessagingService.TYPE_TASK_ASSIGNED)) {
            String userId = message.getString("userId");
            String username = message.getString("username");
            String taskId = message.getString("taskId");
            String taskTitle = message.getString("taskTitle");
            mContext.showToast(username + " selected task " + taskTitle);
            Log.i(App.TAG, mGameInstance.getId() + " task assigned: " + username + " - " + taskTitle);
            mGameInstance.addTaskAssignment(new UserToTask(userId, taskId, mGameInstance.getId()));
        } else if (type.equals(MessagingService.TYPE_STATE_CHANGED)) {
            Integer state = message.getInt("state");
            mGameInstance.setState(state);
            Log.i(App.TAG, mGameInstance.getId() + " state changed " + mGameInstance.getState());
        }else if(type.equals(MessagingService.TYPE_USER_JOINED_GAME)){
            mGameInstance.joinGame(message.getString("userId"));
        }else if(type.equals(MessagingService.TYPE_COLLECTIBLE_COLLECTED)){
            mGameInstance.markCollectibleAsCollected(message.getString("collectibleId"));
        }else if(type.equals(MessagingService.TYPE_TASK_COMPLETED)){
            mGameInstance.markTaskAsCompleted(message.getString("taskId"),message.getDouble("points"));
        }
        updateGameState();
    }

    public void updateGameState() {
        if (mGameInstance.updateState(mUsers)) {
            MessagingService.sendStateUpdatedMessage(mGameInstance.getId(), mGameInstance.getState(),mContext.mApp.getCurrentUser().getId());
        }
    }

}
