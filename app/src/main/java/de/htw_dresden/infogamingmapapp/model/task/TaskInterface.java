package de.htw_dresden.infogamingmapapp.model.task;

/**
 * Created by vanka on 09.01.2017.
 */

public interface TaskInterface {
    void onTaskCompleted(Double pointsReceived);
    void notifyTasksUpdated();
}
