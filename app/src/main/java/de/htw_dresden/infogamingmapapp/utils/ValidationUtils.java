package de.htw_dresden.infogamingmapapp.utils;

/**
 * Created by vanka on 11.12.2016.
 */

public class ValidationUtils {

    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean validateNotEmpty(String... data){
        for(String s: data){
            if(s.isEmpty()){
                return false;
            }
        }
        return true;
    }
}
