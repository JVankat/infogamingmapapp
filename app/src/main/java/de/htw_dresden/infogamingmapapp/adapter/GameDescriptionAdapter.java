package de.htw_dresden.infogamingmapapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.model.GameDescription;
import de.htw_dresden.infogamingmapapp.utils.LocationUtils;

/**
 * Created by vanka on 19.12.2016.
 */

public class GameDescriptionAdapter extends ArrayAdapter<GameDescription> {

    List<GameDescription> mGameDescriptions;
    LatLng mLocation;

    public GameDescriptionAdapter(Context context, int resource, List<GameDescription> objects, LatLng location) {
        super(context, resource, objects);
        mGameDescriptions = objects;
        mLocation = location;
    }

    @Nullable
    @Override
    public GameDescription getItem(int position) {
        return mGameDescriptions.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.game_description_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.gameNameTv);
            viewHolder.description = (TextView) convertView.findViewById(R.id.gameDescriptionTv);
            viewHolder.distance = (TextView) convertView.findViewById(R.id.distanceTv);
            viewHolder.timeLimit = (TextView) convertView.findViewById(R.id.timeLimitTv);
            viewHolder.minPlayers = (TextView) convertView.findViewById(R.id.minPlayersTv);
            viewHolder.maxPlayers = (TextView) convertView.findViewById(R.id.maxPlayersTv);
            viewHolder.humanReadableLocation = (TextView) convertView.findViewById(R.id.humanReadableLocationTv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        GameDescription gameDescription = getItem(position);
        if (gameDescription != null) {
            viewHolder.name.setText(gameDescription.getTitle());
            viewHolder.description.setText(gameDescription.getDescription());
            if (gameDescription.getLatitude() != null && gameDescription.getLongitude() != null && mLocation!=null) {
                LatLng latLng = new LatLng(gameDescription.getLatitude(),gameDescription.getLongitude());
                double latLngDistance = LocationUtils.getLatLngDistance(mLocation, latLng);
                viewHolder.distance.setText("Start distance: " + latLngDistance + " m");
            }
            viewHolder.timeLimit.setText("Max duration: " + (gameDescription.getTimeLimitSeconds()/60)+" minutes");
            viewHolder.minPlayers.setText(String.format("Min players: %d", gameDescription.getMinPlayers()));
            viewHolder.maxPlayers.setText(String.format("Max players: %d", gameDescription.getMaxPlayers()));
            if(gameDescription.getHumanReadableStartLocation()!=null){
                viewHolder.humanReadableLocation.setText(gameDescription.getHumanReadableStartLocation());
            }
        }

        return convertView;
    }


    private static class ViewHolder {
        TextView name, description, distance, timeLimit, minPlayers, maxPlayers, humanReadableLocation;
    }

}
