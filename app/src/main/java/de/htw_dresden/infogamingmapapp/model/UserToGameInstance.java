package de.htw_dresden.infogamingmapapp.model;

import java.io.Serializable;

/**
 * Created by vanka on 20.01.2017.
 */

public class UserToGameInstance implements Serializable {

    private String objectId;
    private String userId;
    private String gameId;

    public UserToGameInstance() {

    }

    public UserToGameInstance(String userId, String gameId) {
        this.userId = userId;
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
