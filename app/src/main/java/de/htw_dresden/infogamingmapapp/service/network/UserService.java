package de.htw_dresden.infogamingmapapp.service.network;

import android.content.Context;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.exceptions.BackendlessException;
import com.backendless.persistence.BackendlessDataQuery;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.model.UserToGameInstance;
import de.htw_dresden.infogamingmapapp.model.UserToTask;
import de.htw_dresden.infogamingmapapp.utils.SharedPreferenceHelper;

/**
 * Created by vanka on 11.12.2016.
 */

public class UserService {

    private UserService() {

    }

    public static User login(String email, String password) throws BackendlessException {
        BackendlessUser user = Backendless.UserService.login(email, password);
        Backendless.UserService.setCurrentUser(user);
        if (user != null) {
            return new User(user);
        }
        return null;
    }

    public static User register(String username, String email, String password, LatLng location) throws BackendlessException {
        BackendlessUser user = new BackendlessUser();
        user.setEmail(email);
        user.setPassword(password);
        user.setProperty("name", username);
        if (location != null) {
            user.setProperty("latitude", location.latitude);
            user.setProperty("longitude", location.longitude);
        }
        BackendlessUser registeredUser = Backendless.UserService.register(user);
        if (registeredUser != null) {
            return new User(registeredUser);
        }
        return null;
    }

    public static void logout(Context context) throws BackendlessException {
        SharedPreferenceHelper.clearEmail(context);
        SharedPreferenceHelper.clearPassword(context);
        Backendless.UserService.logout();
    }


    public static void updateUserLocation(User user, LatLng location) throws BackendlessException {
        if (location != null && Backendless.UserService.CurrentUser() != null) {
            Backendless.UserService.CurrentUser().setProperty("latitude", location.latitude);
            Backendless.UserService.CurrentUser().setProperty("longitude", location.longitude);
            Backendless.UserService.update(Backendless.UserService.CurrentUser());
        }
    }

    public static Collection<User> getAllUSers() throws BackendlessException {
        Collection<User> users = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        BackendlessCollection<BackendlessUser> data = Backendless.Data.of(BackendlessUser.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (BackendlessUser user : data.getCurrentPage()) {
                users.add(new User(user));
            }
            data = data.nextPage();
        }
        return users;
    }

    /**
     * Get users with certain ids
     *
     * @param userIds collection of ids to filter by
     * @return collection of users
     */
    public static Collection<User> getUsers(Collection<String> userIds) {
        List<User> users = new ArrayList<>();
        for (User user : getAllUSers()) {
            if (userIds.contains(user.getId())) {
                users.add(user);
            }
        }
        return users;
    }

    public static void joinGame(String userId, String gameId) throws BackendlessException {
        UserToGameInstance userToGameInstance = new UserToGameInstance(userId, gameId);
        Backendless.Persistence.of(UserToGameInstance.class).save(userToGameInstance);
    }

    public static void leaveGame(String userId, String gameId) throws BackendlessException {
        Collection<UserToGameInstance> usersOfGame = UserService.getUserToGameInstances("userId = '"+userId+"' AND gameId = '"+gameId+"'");
        for(UserToGameInstance userOfGame: usersOfGame){
            Backendless.Persistence.of(UserToGameInstance.class).remove(userOfGame);
        }
    }

    public static Collection<UserToGameInstance> getAllUserToGameInstances() {
        return getUserToGameInstances(null);
    }

    public static Collection<UserToGameInstance> getUserToGameInstances(String queryString) {
        Collection<UserToGameInstance> userToGameInstances = new ArrayList<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        if (queryString != null) {
            query.setWhereClause(queryString);
        }
        query.setPageSize(80);
        BackendlessCollection<UserToGameInstance> data = Backendless.Data.of(UserToGameInstance.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (UserToGameInstance userToGameInstance : data.getCurrentPage()) {
                userToGameInstances.add(userToGameInstance);
            }
            data = data.nextPage();
        }
        return userToGameInstances;
    }

    public static Collection<String> getUsersOfGame(String gameId) {
        return getUsersOfGame(gameId, getUserToGameInstances("gameId = '" + gameId + "'"));
    }

    public static Collection<String> getUsersOfGame(String gameId, Collection<UserToGameInstance> userToGameInstances) {
        Set<String> userIds = new HashSet<>();
        for (UserToGameInstance userToGameInstance : userToGameInstances) {
            if (userToGameInstance.getGameId().equals(gameId)) {
                userIds.add(userToGameInstance.getUserId());
            }
        }
        return userIds;
    }

    public static void assignTask(String userId, String taskId, String gameId) {
        Backendless.Persistence.of(UserToTask.class).save(new UserToTask(userId, taskId, gameId));
    }

    public static Collection<String> getTasksOfUser(String userId, String gameId) {
        Set<String> tasksOfUser = new HashSet<>();
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setPageSize(80);
        query.setWhereClause("userId = '" + userId + "' AND gameId = '" + gameId + "'");
        BackendlessCollection<UserToTask> data = Backendless.Data.of(UserToTask.class).find(query);
        while (data.getCurrentPage().size() > 0) {
            for (UserToTask userToTask : data.getCurrentPage()) {
                if (userToTask.getGameId().equals(gameId) && userToTask.getUserId().equals(userId)) {
                    tasksOfUser.add(userToTask.getTaskId());
                }
            }
            data = data.nextPage();
        }
        return tasksOfUser;
    }

}
