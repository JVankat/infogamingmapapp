package de.htw_dresden.infogamingmapapp;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import de.htw_dresden.infogamingmapapp.model.Team;
import de.htw_dresden.infogamingmapapp.service.network.TeamService;
import de.htw_dresden.infogamingmapapp.utils.ValidationUtils;

@EActivity
public class CreateTeamActivity extends BaseActivity {

    EditText mNameEt, mPasswordEt;
    TextInputLayout mPasswordEtHolder;
    Button mCreateTeamButton;
    CheckBox mPrivateCheckbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);
        mNameEt = (EditText) findViewById(R.id.teamNameEt);
        mPasswordEt = (EditText) findViewById(R.id.passwordEt);
        mPasswordEtHolder = (TextInputLayout) findViewById(R.id.passwordEtHolder);
        mPrivateCheckbox = (CheckBox) findViewById(R.id.privateCb);
        mCreateTeamButton = (Button) findViewById(R.id.createTeamBtn);
        if (mPrivateCheckbox.isChecked()) {
            mPasswordEtHolder.setVisibility(View.VISIBLE);
        } else {
            mPasswordEtHolder.setVisibility(View.GONE);
        }

        mPrivateCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (mPrivateCheckbox.isChecked()) {
                    mPasswordEtHolder.setVisibility(View.VISIBLE);
                } else {
                    mPasswordEtHolder.setVisibility(View.GONE);
                }
            }
        });

        mCreateTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInputs()) {
                    saveTeam();
                } else {
                    String message = "";
                    if (!ValidationUtils.validateNotEmpty(mNameEt.getText().toString())) {
                        message += "Team name is empty, you failure!";
                    }
                    if (mPrivateCheckbox.isChecked() && !ValidationUtils.validateNotEmpty(mPasswordEt.getText().toString())) {
                        if (!message.isEmpty()) {
                            message += " And password";
                        } else {
                            message += "Password";
                        }
                        message += " is empty, you wanna create a private team publicly available? Your father must be proud!";
                    }
                    showDialog(message);
                }
            }
        });
    }

    @Background
    public void saveTeam() {
        Team team = mPrivateCheckbox.isChecked() ? new Team(mNameEt.getText().toString(), mPasswordEt.getText().toString()) : new Team(mNameEt.getText().toString());
        Team savedTeam = TeamService.saveTeam(team);
        TeamService.joinTeam(mApp.getCurrentUser().getId(), savedTeam.getId());
        onTeamSaved(savedTeam);
    }

    @UiThread
    public void onTeamSaved(Team savedTeam) {
        String message = "Team " + savedTeam.getName() + " has been created and is " + (savedTeam.isPublic() ? "public" : "private") + ".";
        Log.i(App.TAG, message);
        showToast(message);
        finish();
    }

    private boolean validateInputs() {
        return ValidationUtils.validateNotEmpty(mNameEt.getText().toString()) && (!mPrivateCheckbox.isChecked() || ValidationUtils.validateNotEmpty(mPasswordEt.getText().toString()));
    }
}
