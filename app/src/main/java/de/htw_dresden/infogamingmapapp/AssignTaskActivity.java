package de.htw_dresden.infogamingmapapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.backendless.messaging.Message;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.htw_dresden.infogamingmapapp.adapter.TaskAdapter;
import de.htw_dresden.infogamingmapapp.model.GameInstance;
import de.htw_dresden.infogamingmapapp.model.User;
import de.htw_dresden.infogamingmapapp.model.UserToTask;
import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.service.MessagingService;
import de.htw_dresden.infogamingmapapp.service.network.UserService;

@EActivity
public class AssignTaskActivity extends BaseActivity {

    List<Task> mTasks;
    TaskAdapter mTaskAdapter;
    ListView mTaskLv;
    FunkyGameInstanceUpdater mUpdater;
    Button mFinishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_task);

        mCurrentGame = (GameInstance) getIntent().getSerializableExtra("gameInstance");
        mTasks = new ArrayList<>(mCurrentGame.getGameDescription().getTasks());
        try {
            mUsers = (List<User>) getIntent().getSerializableExtra("users");
        } catch (Exception e) {
            e.printStackTrace();
            mUsers = new ArrayList<>();
        }

        subscribeToChannel();

        mTaskLv = (ListView) findViewById(R.id.taskList);

        mUpdater = new FunkyGameInstanceUpdater(AssignTaskActivity.this, new ArrayList<User>(), mCurrentGame);

        for (UserToTask taskAssignment : mCurrentGame.getTaskAssignments()) {
            for (Task task : mTasks) {
                if (task.getId().equals(taskAssignment.getTaskId())) {
                    String usernameOrId = taskAssignment.getUserId();
                    if (mUsers != null) {
                        for (User user : mUsers) {
                            if (user.getId().equals(taskAssignment.getUserId())) {
                                usernameOrId = user.getUsername();
                                break;
                            }
                        }
                    }
                    task.setOwner(usernameOrId);
                }
            }
        }
        mTaskAdapter = new TaskAdapter(AssignTaskActivity.this, R.layout.task_item, mTasks);
        mTaskLv.setAdapter(mTaskAdapter);
        mTaskLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Task task = mTaskAdapter.getItem(i);
                if (task != null) {
                    if (task.getOwner() != null) {
                        showToast("Sorry, this task is already assigned.");
                    } else if (!mCurrentGame.getCurrentTasksOfUser(mApp.getCurrentUser().getId()).isEmpty() && mCurrentGame.getPlayerIds().size() >= mCurrentGame.getGameDescription().getTasks().size()) {
                        showToast("Sorry, you cuntnot choose more than 1 task for this game.");
                    } else {
                        task.setOwner(mApp.getCurrentUser().getUsername());
                        assignTask(mApp.getCurrentUser().getId(), task.getId(), mCurrentGame.getId(), mApp.getCurrentUser().getUsername(), task.getName());
                        mTaskAdapter.notifyDataSetChanged();
                    }
                } else {
                    showToast("Ouch, we've failed!");
                }
            }
        });
        mFinishButton = (Button) findViewById(R.id.taskSelectedBtn);
        mFinishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("gameInstance", mCurrentGame);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Background
    public void assignTask(String userId, String taskId, String gameId, String username, String taskName) {
        messageTaskAssigned(userId, taskId, gameId, username, taskName);
        UserService.assignTask(userId, taskId, gameId);
        mCurrentGame.addTaskAssignment(new UserToTask(userId, taskId, gameId));
    }

    @UiThread
    public void messageTaskAssigned(String userId, String taskId, String gameId, String username, String taskName){
        MessagingService.sendTaskAssignedMessage(gameId, userId, taskId, username, taskName);

    }

    @UiThread
    @Override
    public void handleMessages(List<Message> messages) {
        for (Message message : messages) {
            try {
                JSONObject data = new JSONObject(message.getData().toString());
                if (!data.getString("gameId").equals(mCurrentGame.getId())) {
                    continue;
                }
                if (data.getString("type").equals("task_assigned")) {
                    String taskId = data.getString("taskId");
                    for (Task task : mTasks) {
                        if (task.getId().equals(taskId)) {
                            task.setOwner(data.getString("username"));
                        }
                    }
                    mTaskAdapter.notifyDataSetChanged();
                }
                mUpdater.digestMessage(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        try {
            messages.clear();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }
    }


}
