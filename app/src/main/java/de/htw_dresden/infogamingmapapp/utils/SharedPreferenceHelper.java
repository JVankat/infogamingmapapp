package de.htw_dresden.infogamingmapapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import de.htw_dresden.infogamingmapapp.App;

/**
 * Created by vanka on 23.11.2016.
 */

public class SharedPreferenceHelper {

    private static final String USERNAME_KEY = "username";
    private static final String PASSWORD_KEY = "password";
    private static final String EMAIL_KEY = "email";

    public static void saveString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(App.PACKAGE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void saveUsername(Context context, String value) {
        saveString(context, USERNAME_KEY, value);
    }

    public static void saveEmail(Context context, String value) {
        saveString(context, EMAIL_KEY, value);
    }

    public static void savePassword(Context context, String value) {
        saveString(context, PASSWORD_KEY, value);
    }

    public static String getString(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(App.PACKAGE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, null);
    }

    public static String getUsername(Context context) {
        return getString(context, USERNAME_KEY);
    }

    public static String getPassword(Context context) {
        return getString(context, PASSWORD_KEY);
    }

    public static String getEmail(Context context) {
        return getString(context, EMAIL_KEY);
    }

    public static void clearEmail(Context context) {
        saveString(context,EMAIL_KEY,null);

    }

    public static void clearPassword(Context context) {
        saveString(context,PASSWORD_KEY,null);
    }
}
