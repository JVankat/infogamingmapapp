package de.htw_dresden.infogamingmapapp.model;

/**
 * Created by vanka on 28.01.2017.
 */

public class CollectedCollectibleToGame {

    private String collectibleId;
    private String gameId;

    public CollectedCollectibleToGame() {

    }

    public CollectedCollectibleToGame(String collectibleId, String gameId) {
        this.collectibleId = collectibleId;
        this.gameId = gameId;
    }

    public String getCollectibleId() {
        return collectibleId;
    }

    public void setCollectibleId(String collectibleId) {
        this.collectibleId = collectibleId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
