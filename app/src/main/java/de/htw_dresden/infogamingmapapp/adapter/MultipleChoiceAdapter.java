package de.htw_dresden.infogamingmapapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.utils.ViewUtils;

/**
 * Created by vanka on 19.12.2016.
 */

public class MultipleChoiceAdapter extends ArrayAdapter<String> implements Serializable {

    List<String> mOptions;
    List<String> mSelectedOptions;
    Set<String> correctAnswers;
    Context context;

    public MultipleChoiceAdapter(Context context, int resource, List<String> objects, List<String> selectedObjects, Set<String> correctAnswers) {
        super(context, resource, objects);
        this.context = context;
        mOptions = objects;
        mSelectedOptions = selectedObjects;
        this.correctAnswers = correctAnswers;
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return mOptions.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.multiple_choice_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.optionView = (TextView) convertView.findViewById(R.id.optionTv);
            viewHolder.checkmark = (ImageView) convertView.findViewById(R.id.checkmarkIv);
            viewHolder.relativeLayout = (RelativeLayout) convertView.findViewById(R.id.mcWrapperRl);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String option = getItem(position);
        viewHolder.optionView.setText(option);
        if (mSelectedOptions.contains(option)) {
            viewHolder.checkmark.setVisibility(View.VISIBLE);
        } else {
            viewHolder.checkmark.setVisibility(View.INVISIBLE);
        }
        if (!correctAnswers.isEmpty()) {
            viewHolder.optionView.setTextColor(ViewUtils.getColor(context, android.R.color.white));
            if (correctAnswers.contains(option)) {
                viewHolder.relativeLayout.setBackgroundColor(ViewUtils.getColor(context, android.R.color.holo_green_dark));
            } else {
                viewHolder.relativeLayout.setBackgroundColor(ViewUtils.getColor(context, android.R.color.holo_red_dark));
            }
        } else {
            viewHolder.optionView.setTextColor(ViewUtils.getColor(context, android.R.color.black));
            viewHolder.relativeLayout.setBackgroundColor(ViewUtils.getColor(context, android.R.color.white));
        }

        return convertView;
    }


    private static class ViewHolder {
        private TextView optionView;
        private ImageView checkmark;
        private RelativeLayout relativeLayout;
    }

}
