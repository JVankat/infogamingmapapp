package de.htw_dresden.infogamingmapapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.messaging.Message;
import com.backendless.services.messaging.MessageStatus;

import org.androidannotations.annotations.EFragment;

import java.util.List;

import de.htw_dresden.infogamingmapapp.App;
import de.htw_dresden.infogamingmapapp.R;
import de.htw_dresden.infogamingmapapp.adapter.TaskAdapter;
import de.htw_dresden.infogamingmapapp.model.task.Task;
import de.htw_dresden.infogamingmapapp.model.task.TaskInterface;
/**
 * Created by tuxflo on 14.01.17.
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TaskFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TaskFragment extends Fragment implements TaskInterface {
    private List<Task> mTasks;
    private TaskAdapter mAdapter;
    private App mApp;

    private OnFragmentInteractionListener mListener;

    public TaskFragment() {
        // Required empty public constructor
    }

    public void setSubscription() {
        Backendless.Messaging.subscribe(
                new AsyncCallback<List<Message>>() {
                    public void handleResponse(List<Message> response) {
                        for (Message message : response) {
                            String publisherId = message.getPublisherId();
                            Object data = message.getData();
                            Log.i(App.TAG, "Response: " + data.toString());
                            mAdapter.setOwner("Flo", 1);
                            mAdapter.notifyDataSetChanged();
                        }
                    }

                    public void handleFault(BackendlessFault fault) {
                        Log.i(App.TAG, "Fault: " + fault.getMessage());
                    }
                },
                new AsyncCallback<Subscription>() {
                    public void handleResponse(Subscription response) {
                        Log.i(App.TAG, "SubID: " + response.getSubscriptionId());
                    }

                    public void handleFault(BackendlessFault fault) {
                        Log.i(App.TAG, "Fault: " + fault.getMessage());
                    }
                }
        );
    }

    public static TaskFragment newInstance(App app, List<Task> tasks) {
        TaskFragment fragment = new TaskFragment();
        fragment.mApp = app;
        fragment.mTasks = tasks;
        Log.i(App.TAG, "before subscribe...");
        fragment.setSubscription();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new TaskAdapter(getContext(), R.layout.task_item, mTasks);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_task, container, false);
        ListView lv = (ListView) view.findViewById(R.id.taskList);
        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //adapterView.getChildAt(i).setBackgroundColor(Color.BLACK);
                final Task task = mAdapter.getItem(i);
                //for(Task t : mTasks) {
                //    if(t.equals(mAdapter.getItem(i))) {
                //        Log.i(App.TAG, "equals is true");
                //    }
                //    else
                //        adapterView.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                task.setOwner("flo");
                mAdapter.notifyDataSetChanged();
                Log.i(App.TAG, "item clicked..." + i);
                Log.i(App.TAG, "Sending message...");
                Backendless.Messaging.publish( "default", "Selection" + "Flo:" + i, new AsyncCallback<MessageStatus>()
                {
                    @Override
                    public void handleResponse( MessageStatus messageStatus )
                    {
                        Log.i(App.TAG, "Message published - " + messageStatus.getMessageId() );
                    }

                    @Override
                    public void handleFault( BackendlessFault backendlessFault )
                    {
                        Log.i(App.TAG, "Server reported an error " + backendlessFault.getMessage() );
                    }
                } );
                if (task == null) {
                    mListener.showToast("Sorry, task seems to be unavailable.");
                    return;
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void notifyTasksUpdated() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        } else {
            Log.e(App.TAG, "Task adapter is null");
            onDestroyView();
        }
    }

    @Override
    public void onTaskCompleted(Double pointsReceived) {

    }

    public interface OnFragmentInteractionListener {
        void showToast(String message);
    }
    public List<Task> getTasks() {
        return this.mTasks;
    }

    public void sendSelection(int i) {
    }
}
