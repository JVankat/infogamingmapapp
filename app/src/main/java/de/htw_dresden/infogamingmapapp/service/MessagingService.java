package de.htw_dresden.infogamingmapapp.service;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.messaging.PublishOptions;
import com.backendless.services.messaging.MessageStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vanka on 26.01.2017.
 */

public class MessagingService {

    public static final String TYPE_TASK_ASSIGNED = "task_assigned";
    public static final String TYPE_STATE_CHANGED = "state_changed";
    public static final String TYPE_USER_JOINED_GAME = "joined_game";
    public static final String TYPE_COLLECTIBLE_COLLECTED = "collectible_collected";
    public static final String TYPE_TASK_COMPLETED = "task_completed";

    private MessagingService() {

    }

    private static JSONObject createMessage(String gameId, String messageType) throws JSONException {
        JSONObject message = new JSONObject();
        message.put("gameId", gameId);
        message.put("type", messageType);
        return message;
    }

    public static void sendTaskAssignedMessage(String gameId, String userId, String taskId, String username, String taskTitle) {
        try {
            JSONObject message = createMessage(gameId, TYPE_TASK_ASSIGNED);
            message.put("userId", userId);
            message.put("taskId", taskId);
            message.put("username", username);
            message.put("taskTitle", taskTitle);
            publishMessage(gameId, message, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendStateUpdatedMessage(String gameId, Integer state, HashMap<String, Object> data, String userId) {
        try {
            JSONObject message = createMessage(gameId, TYPE_STATE_CHANGED);
            message.put("state", state);
            if (data != null) {
                for (Map.Entry<String, Object> entry : data.entrySet()) {
                    message.put(entry.getKey(), entry.getValue());
                }
            }
            publishMessage(gameId, message, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendStateUpdatedMessage(String gameId, Integer state, String userId) {
        sendStateUpdatedMessage(gameId, state, null, userId);
    }

    private static void publishMessage(String channelId, JSONObject message, String userId) {
        try {
            publishMessage(channelId, message.toString(), userId);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    private static void publishMessage(String channelId, String message, String userId) {
//        Backendless.Messaging.publish(channelId, message, new PublishOptions());
        try {
            PublishOptions publishOptions = new PublishOptions();
            AsyncCallback<MessageStatus> responder = null;
            publishOptions.setPublisherId(userId);
//            try {
//                responder = new AsyncCallback<MessageStatus>() {
//                    @Override
//                    public void handleResponse(MessageStatus response) {
//                        try {
////                        Log.i(App.TAG, "Message saved - " + response.getMessageId());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    @Override
//                    public void handleFault(BackendlessFault fault) {
//                        try {
////                        Log.i(App.TAG, "Message not saved - " + fault.getDetail());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                };
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            Backendless.Messaging.publish(channelId, message, publishOptions, responder);
        } catch (ClassCastException e) { //FIXME: why is fucking Backendless throwing fucking ClassCastException (ain't they gettin' their shit done?)
            e.printStackTrace();
        }

    }

    public static void sendUserJoinedGameMessage(String userId, String gameId) {
        try {
            JSONObject message = createMessage(gameId, TYPE_USER_JOINED_GAME);
            message.put("userId", userId);
            publishMessage(gameId, message, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendCollectibleCollectedMessage(String gameId, String collectibleId, String userId) {
        try {
            JSONObject message = createMessage(gameId, TYPE_COLLECTIBLE_COLLECTED);
            message.put("collectibleId", collectibleId);
            publishMessage(gameId, message, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendTaskCompletedMessage(String taskId, String gameId, Double points, String userId) {
        try {
            JSONObject message = createMessage(gameId, TYPE_TASK_COMPLETED);
            message.put("taskId", taskId);
            message.put("points", points);
            publishMessage(gameId, message, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
