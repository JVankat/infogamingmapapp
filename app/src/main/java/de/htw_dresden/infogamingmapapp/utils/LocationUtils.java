package de.htw_dresden.infogamingmapapp.utils;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by vanka on 14.11.2016.
 */

public class LocationUtils {

    public static double degreeToRadian(double degree) {
        return (degree * Math.PI / 180.0);
    }

    public static double radianToDegree(double radian) {
        return (radian * 180.0 / Math.PI);
    }

    /**
     * Calculate distance between two points given by <code>LatLng</code>.
     *
     * @return the distance in m
     */
    public static double getLatLngDistance(LatLng p1, LatLng p2) {
        double theta = p1.longitude - p2.longitude;
        double dist = Math.sin(degreeToRadian(p1.latitude)) * Math.sin(degreeToRadian(p2.latitude)) + Math.cos(degreeToRadian(p1.latitude)) * Math.cos(degreeToRadian(p2.latitude)) * Math.cos(degreeToRadian(theta));
        dist = Math.acos(dist);
        dist = radianToDegree(dist);
        dist = dist * 60 * 1.1515 * 1000;
        float[] result = new float[2];
        Location.distanceBetween(p1.latitude, p1.longitude, p2.latitude, p2.longitude, result);
        return result[0];
    }
}
